DataTransformers FiducialCore: convertisseurs de données pour composants de formulaires Symfony2
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [FormTypes disponibles](#toc_3)
    - [ChoiceToNullableValueTransformer](#toc_4): pour support de valeur nulle dans attribut mappé d'une entité
    - [EntityToIdTransformer](#toc_5): pour support de transformation d'une entité vers son Id

    
Présentation:
-------------

- Le FiducialCore est livré avec quelques convertisseurs de données Symfony2 assez utiles pour vos formulaires, et surtout réutilisables.
- Ceux-ci respecent la logique d'implémentation Symfony2, [présentée ici](http://symfony.com/fr/doc/current/cookbook/form/data_transformers.html)
- Les classes object PHP répondant a ces services sont situées dans **Fiducial\CoreBundle\Form\DataTransformer**

DataTransformers disponibles
---------------------

#### ChoiceToNullableValueTransformer:
- But: pouvoir supporter une valeur nulle dans attribut mappé d'une entité vers une autre entité lors de traitement de formulaires
- Exemple:
    
```
<?php
namespace Fiducial\CoreBundle\Form\Type;

use Fiducial\CoreBundle\Form\DataTransformer\ChoiceToNullableValueTransformer;

/**
 * Class NullableEntityType
 * @package Fiducial\CoreBundle\Form\Type
 */
class NullableEntityType extends BaseEntityType
{

    //...

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //parent form building
        parent::buildForm($builder,$options);
        //replace view transformer
        $builder->resetViewTransformers();
        $transformer = new ChoiceToNullableValueTransformer(
            $options['choice_list'],
            $options['em'],
            $options['class']
        );
        $builder->addViewTransformer($transformer,true);
    }
    
    //...

}
```
    
#### EntityToIdTransformer:
- But: pouvoir transformer une entité en id lors de traitement de formulaires
- Exemple:
    
```
<?php
namespace Fiducial\CoreBundle\Form\Type;

use Fiducial\CoreBundle\Form\DataTransformer\EntityToIdTransformer;

/**
 * Class HiddenEntity
 * @package Fiducial\CoreBundle\Form\Type
 */
class HiddenEntityType extends AbstractType
{
    
    //...

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new EntityToIdTransformer(
            $this->objectManager,
            $options['class']
        );
        $builder->addModelTransformer($transformer);
    }
    
    //...
    
}
```
