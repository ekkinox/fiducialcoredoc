FormTypes FiducialCore: composants de formulaires Symfony2
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [FormTypes disponibles](#toc_3)
    - [fiducial_core_nullable_entity](#toc_4): formType pour lien entité nullable
    - [fiducial_core_soft_deletable_entity](#toc_5): formType pour lien entité SoftDeletable
    - [fiducial_core_hidden_entity](#toc_6): formType pour lien entité en input hidden 
    - [fiducial_core_file](#toc_7): formType pour filechooser style boostrap3
    - [fiducial_core_wysiwyg](#toc_8): formType pour éditeur WYSIWYG
    

Présentation:
-------------

- Le FiducialCore est livré avec quelques formTypes Symfony2 (composants de formulaires) assez utiles, et surtout réutilisables.
- La déclaration de ces formTypes est faite dans les fichier **Fiducial\CoreBundle\Resources\config\Services\form.yml**
- Ce sont donc des services que vous pouver **tous surcharger à volonté** grace à la mécanique de classe paramètrable (voir chapitre sur les [redéfinitions de services](../services/).)
- Leur portage en tant que service permet de les apeller dans les formBuilders de vos applications directement par leur alias (tag : *form.type*)
- Les classes object PHP répondant a ces services sont situées dans **Fiducial\CoreBundle\Form\Type**

FormTypes disponibles (ids de services)
---------------------

#### fiducial_core.form.type.fiducial_core_nullable_entity:
- But: pouvoir offrir un selecteur d'entité liée nullable (foreign key = null) dans une relation ManyToOne bilatérale (mapped + inversed)
- Alias: **fiducial_core_nullable_entity**
- DataTransformer : *ChoiceToNullableValueTransformer*
- Dependances : voir [formType entity de Symfony2](http://symfony.com/fr/doc/current/reference/forms/types/entity.html)
- Exemple:
    
```
<?php

$builder->add(
    'businessUnit',
    'fiducial_core_nullable_entity',
    array(
        'class'         => 'Fia\RepositoryBundle\Entity\BusinessUnit',
        'property'      => 'name',
        'empty_value'   => '',
        'auto_initialize' => false,
        'label'         => 'Business Unit',
        'required'      => true,
        //...
    )
);
```
    
#### fiducial_core.form.type.fiducial_core_soft_deletable_entity:
- But: pouvoir offrir un selecteur d'entité liée de type **SoftDeletable** avec la capacité de préservation de lien même en cas d'inactivité de l'entité liée
- Alias: **fiducial_core_soft_deletable_entity**
- Dependances : *fiducial_core_nullable_entity* et donc [SF2 entityType](http://symfony.com/fr/doc/current/reference/forms/types/entity.html)
- Exemple:
    
```
<?php

$builder->add(
    'article',
    'fiducial_core_soft_deletable_entity',
    array(
        'class' => 'Fia\RepositoryBundle\Entity\Article',
        'required' => true,
        //...
    )
);
```

#### fiducial_core.form.type.fiducial_core_hidden_entity:
- But: pouvoir offrir la possibilité de lier une entité dans un input type hidden dans un formulaire
- Alias: **fiducial_core_soft_deletable_entity**
- DataTransformer : *EntityToIdTransformer*
- Dependances : voir [formType hidden de Symfony2](http://symfony.com/fr/doc/current/reference/forms/types/hidden.html)
- Exemple:
    
```
<?php

$builder->add(
    'workorder',
    'fiducial_core_hidden_entity',
    array(
        'class' => 'Fia\WorkorderBundle\Entity\Workorder',
        //...
    )
);
```

#### fiducial_core.form.type.fiducial_core_file:
- But: pouvoir offrir un filechooser avec un skin bootstrap3, adapté au reste du skin FiducialCore
- Alias: **fiducial_core_file**
- Dependances : voir [formType file de Symfony2](http://symfony.com/fr/doc/current/reference/forms/types/file.html)
- Exemple:
    
```
<?php

$builder->add(
    'file',
    'fiducial_core_file',
    array(
        'required'=>false,
        //...
    )
);
```

#### fiducial_core.form.type.fiducial_core_wysiwyg:
- But: pouvoir offrir un éditeur WYSIWYG ( basé sur [jQueryTE](http://jqueryte.com/) )
- Alias: **fiducial_core_wysiwyg**
- Dependances : voir [formType textarea de Symfony2](http://symfony.com/fr/doc/current/reference/forms/types/textarea.html)
- Exemple:
    
```
<?php

$builder->add(
    'comment',
    'fiducial_core_wysiwyg',
    array(
        'required'=>false,
        //...
    )
);
```
    



