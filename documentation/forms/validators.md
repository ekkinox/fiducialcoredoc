Constraints validator FiducialCore: Contraintes de validation 
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [Créer une contrainte et son validator dans FiducialCore](#toc_3)
- [Utiliser une contrainte FiducialCore](#toc_4)
- [Contraintes disponibles dans FiducialCore](#toc_5)

Présentation:
-------------

- Le FiducialCore est livré avec quelques contraintes pour vos formulaires. Elles sont facilement utilisables via les annotations.
- Les annotations sont stockées dans **Fiducial\CoreBundle\Annotations\Validator\Constraints**
- Les validators sont stockés dans **Fiducial\CoreBundle\Form\Validator\Constraints**

Créer une contrainte et son validator dans FiducialCore
---------------------

- Suivez la documentation de Symfony2 : [Créer une Contrainte de Validation Personnalisée](http://symfony.com/fr/doc/current/cookbook/validation/custom_constraint.html)

- Il faut que votre contrainte étende celle du *FiducialCore*.
- L'annotation doit être stockée dans **Fiducial\CoreBundle\Annotations\Validator\Constraints**
- Le validator doit être stocké dans **Fiducial\CoreBundle\Form\Validator\Constraints**

- **Exemple de contrainte**: Siret *(pour @Siret())* :

```
<?php

namespace Fiducial\CoreBundle\Annotations\Validator\Constraints;

use Fiducial\CoreBundle\Annotations\Validator\Constraint as FiducialCoreConstraint;

/**
 * @Annotation
 */
final class Siret extends FiducialCoreConstraint
{
    /**
     * @var string
     */
    public $message = 'Siret is not valid';

}
```

- En effet, le répertoire où se trouve les validator est **Fiducial\CoreBundle\Form\Validator\Constraints** et la
classe FiducialCoreConstraint trouvera le bon chemin pour vous.

```
<?php

namespace Fiducial\CoreBundle\Annotations\Validator;

use Symfony\Component\Validator\Constraint as BaseConstraint;
use Fiducial\CoreBundle\Annotations\Validator\ConstraintInterface;

/**
 * @Annotation
 */
abstract class Constraint extends BaseConstraint
{

    /**
     * Constants
     */
    const VALIDATOR_PATH = 'Fiducial\\CoreBundle\\Form\\Validator\\Constraints\\';
    const VALIDATOR_SUFFIX = 'Validator';

    /**
     * @inheritdoc
     */
    public function validatedBy()
    {
        return self::VALIDATOR_PATH.$this->getClassBaseName().self::VALIDATOR_SUFFIX;
    }

    /**
     * Gets classname (even for childs)
     * @return string
     */
    public function getClassBaseName()
    {
        $classNameTmp = explode('\\', get_class($this));

        return array_pop($classNameTmp);
    }
}
```

- **Validator associé**: SiretValidator

```
<?php

namespace Fiducial\CoreBundle\Form\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class SiretValidator
 * @package Fiducial\CoreBundle\Form\Validator\Constraints
 */
class SiretValidator extends ConstraintValidator
{

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$this->siretValidator($value)) {
            $this
                ->context
                ->addViolation(
                    $constraint->message,
                    array('%string%' => $value)
                );
        }
    }

    /**
     * Validates a SIRET code (french company location unique identifier). The
     * sanitization will strip spaces to return a 14 chars length string.
     *
     * @param string &$value Value to validate
     * @param bool $sanitize Sanitize value?
     *
     * @return bool
     */
    public function siretValidator(&$value, $sanitize = true)
    {
        $normalizedValue = str_replace(' ', '', $value);
        if (!ctype_digit($normalizedValue) || 14 !== strlen($normalizedValue)) {
            return false;
        }

        // We take every number one by one
        // if its index (position in the chain begins to 0 in first character) is even
        // we double his value and if it's superior to 9, we deduct 9, we add this value to the total sum

        $sum = 0;
        for ($i = 0; $i < 14; $i++) {
            $number = (int)$normalizedValue[$i];
            if (($i % 2) === 0) {
                if (($number *= 2) > 9) {
                    $number -= 9;
                }
            }
            $sum += $number;
        }
        // the number is valid if numbers's sum is a multiple of 10
        if (($sum % 10) !== 0) {
            return false;
        }

        if ($sanitize) {
            $value = $normalizedValue;
        }

        return true;
    }
}
```

Utiliser une contrainte du FiducialCore
---------------------
  
- Les contraintes du FiducialCore sont disponible via les annotations. 
- Dans votre entité, indiquez que vous utilisez le FiducialCore comme suit :

```
use Fiducial\CoreBundle\Annotations\Validator\Constraints as FiducialCoreAssert;
```

- Vous pourrez alors indiquer via l'annotation de la contrainte celle à appliquer sur vos variables.
- Exemple avec la contrainte siret :

```
    /**
     * @var string
     *
     * @ORM\Column(name="SIRET", type="string", length=14)
     *
     * @GRID\Column(visible=false)
     *
     * @FiducialCoreAssert\Siret()
     */
    private $siret;
```

Contraintes disponibles (annotations)
---------------------


- **Siret**:
    - Vérifie la conformité d'un numéro Siret.
    - Documentation : [Rêgles Siren-Siret](http://www.dsi.cnrs.fr/conduite-projet/phasedeveloppement/technique/etude-detaillee/modele-de-donnees/regles-SIREN-SIRET.pdf)
