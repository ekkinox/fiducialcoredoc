Exensions Twig FiducialCore
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [Extensions / fonctions Twig scope interne](#toc_3)
- [Extensions / fonctions Twig scope développeur](#toc_4)
- [Extensions / fonctions Twig scope dashboard](#toc_4)

Présentation:
-------------

- Le FiducialCore est livré avec quelques extensions Twig interessantes qui automatisent certains process
- Trois scopes sont ici présentés:
    - *interne* : extensions / fonctions Twig utilisées en interne par le FiducialCore
    - *développeur* : extensions / fonctions Twig à la disposition du développeur
    - *dashboard* : extensions / fonctions Twig à la disposition du développeur pour utilisation dans le dashboard
- La déclaration de ces extensions Twig est faite dans les fichier **Fiducial\CoreBundle\Resources\config\Services\twig.yml**
- Ce sont donc des services (tag : *twig.extension*) que vous pouver **toutes surcharger à volonté** grace à la mécanique de classe paramètrable (voir chapitre sur les [redéfinitions de services](../services/).)
- Les classes object PHP répondant a ces services sont situées dans **Fiducial\CoreBundle\Twig**

Extensions / fonctions Twig scope interne:
----------------------------------------------------

- *fiducial_core.twig.extension.affixable_sidebar_builder*:
    - But: en charge de la construction de la sidebar mode affix (voir config du FiducialCore pour activer mode affix)
    - Note : **interne au fonctionnement du FiducialCore**, mais surchargeable si necessaire
    - Fonction Twig: **fiducialcore_generate_affixable_sidebar(mode)**
        - *mode* : string, mode de contruction, valeurs possibles : ['begin', 'end']
    - Exemple: `{{ fiducialcore_generate_affixable_sidebar('begin') }}`
    
- *fiducial_core.twig.extension.search_builder*:
    - But: en charge de la construction du formulaire de recherche globale FiducialCore
    - Note : **interne au fonctionnement du FiducialCore**, mais surchargeable si necessaire
    - Fonction Twig: **fiducialcore_generate_search_form**
    - Exemple: `{{ fiducialcore_generate_search_form() }}`
    
- *fiducial_core.twig.extension.entity_actions*:
    - But: en charge de la construction des liens de navigation automatisés dans les Cruds FiducialCore
    - Note : **interne au fonctionnement du FiducialCore**, mais surchargeable si necessaire
    - Fonction Twig: **fiducialcore_generate_entity_actions(readonly)**
        - *readonly*: booléen, pour afficher ou non les routes en mode readonly *(see)* ou non *(see + edit + delete + add)*
    - Exemple: `{{ fiducialcore_generate_entity_actions(true) }}`
    

Extensions / fonctions Twig scope développeur:
----------------------------------------------

- *fiducial_core.twig.extension.entity_archive_status*:
    - But: en charge de la construction des indicateurs automatisés de statut (actif/archivé) pour une entité *SoftDeletable*
    - Fonction Twig: **fiducialcore_render_entity_archive_status(softDeletableEntity)**
        - *softDeletableEntity*: entité doctrine impémentant la mécanique SoftDeletable
    - Exemple: `{{ fiducialcore_render_entity_archive_status(my_entity) }}`
    
- *fiducial_core.twig.extension.entity_uploadable_preview*:
    - But: en charge de la construction du rendu automatisé du fichier (infos, preview, download) pour une entité *Uploadable*
    - Fonction Twig: **fiducialcore_render_entity_uploadable_preview(softDeletableEntity, withDownload, onlyImage)**
        - *softDeletableEntity*: entité doctrine impémentant la mécanique SoftDeletable
        - *withDownload*: booléen, pour afficher ou non dans le bloc un bouton de download du fichier
        - *onlyImage*: booléen, pour n'afficher que l'image sans le bloc complet d'informations
    - Exemple: `{{ fiducialcore_render_entity_uploadable_preview(my_entity, true, false) }}`
    
- *fiducial_core.twig.extension.entity_related_entities*:
    - But: en charge de la construction du bloc de listing des entités liées à l'entité cible.
    - Ce bloc n'affiche que les entités inverses au niveau relation mapping doctrine
    - Ce bloc est chargé en mode *lazyLoad* via Ajax (pour éviter le gaspillage de performances)
    - Fonction Twig: **fiducialcore_render_related_entities(entity, withActions)**
        - *entity*: entité doctrine
        - *withActions*: booléen, pour afficher ou non les actions définies dans les entités inverses
    - Exemple: `{{ fiducialcore_render_related_entities(my_entity, true) }}`
    
- *fiducial_core.twig.extension.format_bytes*:
    - But: permet le formatage d'une taille en bytes de manière humainement compréhensible (ex 1024 bytes affichera 1Mb)
    - Extension Twig: **fiducialcore_format_bytes**
    - Exemple: `{{ entity.size|fiducialcore_format_bytes }}`


Extensions / fonctions Twig scope dashboard:
----------------------------------------------

- *fiducial_core.twig.extension.user_board*:
    - But: permet la contruction d'une carte dashboard pour afficher les informations de l'utilisateur connecté (login, rôles)
    - Fonction Twig: **fiducialcore_generate_user_board()**
    - Exemple: `{{ fiducialcore_generate_user_board() }}`
    
- *fiducial_core.twig.extension.notification_board*:
    - But: permet la contruction d'une carte dashboard pour afficher une zone de lecture des notifications pour l'utilisateur connecté
    - Fonction Twig: **fiducialcore_generate_notification_board(parameters)**
        - *parameters* : array des paramètres a injecter au constructeur, parmi
            - severity : type de zone de lecture de notifications (voir chapitre sur les notifications FiducialCore)
            - board_max_height : taille en pixels de la zone de lecture des notifications
    - Exemple: `{{ fiducialcore_generate_notification_board({'severity' => 'danger', 'board_max_height' => '200'}) }}`

- *fiducial_core.twig.extension.advanced_search_board*:
    - But: permet la contruction d'une carte dashboard pour afficher le formulaire de recherche avancée FiducialCore
    - Fonction Twig: **fiducialcore_generate_advanced_search_board()**
    - Exemple: `{{ fiducialcore_generate_advanced_search_board() }}`
