Sommaire Documentation technique FiducialCore
======

- [Installation](installation/): *instructions d'installation*

- [Architecture](architecture/): *architecture du FiducialCore* 

- [Configuration](configuration/): *liste exhaustive des options de configuration du FiducialCore*  
 
- [Générateurs](generators/)
    - [Générateur Bundle](generators/bundle.md): *générer Bundle Symfony2 compatible FiducialCore*
    - [Générateur Entity (ORM Doctrine)](generators/entity.md): *générer Entité Doctrine compatible FiducialCore*
    - [Générateur CRUD](generators/crud.md): *générer CRUD automatisé compatible FiducialCore*

- [Core et Doctrine](core/)
    - [Contexte Object PHP pour Doctrine](core/context.md): *Contexte Object PHP offert par FiducialCore pour manipulation des entités*
    - [Types d'entités Doctrine](core/entity.md): *Entités Doctrine préparées pour FiducialCore*
    - [Doctrine EntityListener](core/entitylistener.md): *Utilisation du Resolver FiducialCore pour implémentation d'entity listeners doctrine*  

- [Formulaires](forms/)
    - [FormTypes](forms/formtypes.md): *champs de formulaire du FiducialCore*
    - [DataTransformers](forms/datatransformers.md): *convertisseurs de données du FiducialCore*
    - [Validators](forms/validators.md): *Contraintes de validation du FiducialCore*
 
- [Services](services/)
    - [Définition des services](services/definition.md): *Définition et liste des services Symfony2 du FiducialCore*
    - [Redéfinition des services](services/redefine.md): *Surcharger/redéfinir les services Symfony2 du FiducialCore*  
 
- [Grilles](grids/): *Présentation, configuration, utilisation des grilles pour entités Doctrine FiducialCore*

- [Menus](menu/): *Utilisation des menus FiducialCore*

- [Notifications](notifications/): *Utilisation du système de notifications FiducialCore*

- [Moteur de recherche](search/): *Utilisation du moteur de recherche FiducialCore*

- [Twig Extensions](twig/): *Liste des extensions Twig du FiducialCore*

- [Utilisateurs](users/)
    - [Contexte utilisateur](users/users.md): *Contexte de gestion des utilisateurs du FiducialCore*
    - [Configuration du CAS](users/CAS.md): *utiliser le connecteur FiducialUser pour s'authentifier au CAS de Fiducial*
 
- [Erreurs](errors/): *Customiser les pages d'erreur du FiducialCore*

- [Maintenance](maintenance/): *Mode maintenance du FiducialCore*
