Menus FiducialCore
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [Utilisation de listeners pour ajouter une entrée au menu](#toc_3)
- [Gestion automatisée du menu Administrateur](#toc_4)
- [Reorganiser les entrées menu](#toc_5)

Présentation:
-------------

- Le FiducialCore propose la gestion facilitée de menus ( via l'utilisation du vendor [KnpMenu](https://github.com/KnpLabs/KnpMenu) )
- Deux menus sont activés par défaut:
    - le menu **main** (service *fiducial_core.menu.main*): section de menu ou les bundles applicatifs enregistrent leur entrées
    - le menu **profile** (service *fiducial_core.menu.profile*): section de menu dédiée à la gestion du compte utlisateur
- Ces deux menus sont exposés via services (fichier Fiducial\CoreBundle\Resources\config\Services\menu.yml) taggés sous *knp_menu.menu*
- Les classes object PHP impliquées coté FiducialCore sont:
    - *Fiducial\CoreBundle\Event\ConfigureMenuEvent.php*: représente l'evennement utilisé pour l'inscription au menu FiducialCore
    - *Fiducial\CoreBundle\Menu\MainBuilder.php*: en charge de la construction des menus **main** et **profile**
- Un troisieme service est disponible : **fiducial_core.menu_builder**
    - utile pour créer une autre instance de menu
    - une retouche du layout Twig FiducialCore est necessaire pour l'afficher
- **Interets** :
    - pas de retouches du layout FiducialCore pour ajouter ses entrées menu (donc indépendant)
    - simplicité de mise en place, maintenance facilitée
    - si le bundle applicatif est désactivé, son entrée menu aussi


Utilisation de listeners pour ajouter une entrée au menu:
----------------------------------------------------

- Si dans votre bundle applicatif *(ici : Fia\WorkorderBundle)* vous voulez ajouter une entrée menu dans le menu **main** propulsé par FiducialCore, il vous faut:
    - Créer un event listener qui:
        - écoute l'evenement **fiducial_core.menu_configure** en tant que service taggé *kernel.event_listener*
        - et qui implémente la méthode **onMenuConfigure()**, comme suit:
    
    ```
    # Fia\WorkorderBundle\Resources\config\services.yml
    services:
        fia_workorder.configure_menu_listener:
            class: Fia\WorkorderBundle\EventListener\ConfigureMenuListener
            arguments: [ @security.token_storage, @security.authorization_checker ]
            tags:
              - { name: kernel.event_listener, event: fiducial_core.menu_configure, method: onMenuConfigure }
    ```
    
    ```
    <?php
    // Fia\WorkorderBundle\EventListener\ConfigureMenuListener.php
    namespace Fia\WorkorderBundle\EventListener;
    
    use Fiducial\CoreBundle\Event\ConfigureMenuEvent;
    use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
    use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
    
    class ConfigureMenuListener
    {
        /**
         * @var TokenStorage
         */
        private $tokenStorage;
    
        /**
         * @var AuthorizationCheckerInterface
         */
        private $authorizationChecker;
    
        /**
         * @param TokenStorage $tokenStorage
         */
        public function __construct(TokenStorage $tokenStorage, AuthorizationCheckerInterface $authorizationChecker)
        {
            $this->tokenStorage = $tokenStorage;
            $this->authorizationChecker = $authorizationChecker;
        }
    
        /**
         * @param ConfigureMenuEvent $event
         */
        public function onMenuConfigure(ConfigureMenuEvent $event)
        {
            //gets user
            $user = $this->tokenStorage->getToken()->getUser();
            
            //gets menu factory with event
            $menu = $event->getMenu();
            
            //check if user authenticated
            if (is_object($user)) {
    
                //create workorder menu section
                $wOrder = $menu->addChild('Work Orders', array(
                    'icon' => 'inbox',
                    'dropdown' => true,
                    'caret' => true,
                ));
                //workorder management
                $canCreate = $this->authorizationChecker->isGranted('ROLE_ADMIN') || $this->authorizationChecker->isGranted('ROLE_MANAGER');
                if ($canCreate) {
                    $wOrder->addChild('Create Work Order', array('icon' => 'plus-sign', 'route' => 'workorder_add'));
                }
                $wOrder->addChild('Manage Work Orders', array('icon' => 'inbox', 'route' => 'workorder_list'));
    
                //create stats menu section
                $stats = $menu->addChild('Statistics', array(
                    'icon' => 'signal',
                    'dropdown' => true,
                    'caret' => true,
                ));
                //stats management
                $stats->addChild('View statistics', array('icon' => 'signal', 'route' => 'statistics_index'));
            }
        }
    }
    ```
    
    - Nous voyons ici que nous avons crée:
        - Un menu dropdown "Work Orders"
            - avec un sous menu "Create Work Order" accessible que aux roles *ROLE_ADMIN* et *ROLE_MANAGER*
            - et un autre sous menu "Manage Work Orders" (pas de restrictions)
        - Un autre menu dropdown "Statistics"
            - avec un sous menu "View statistics" (pas de restrictions)
            
    - Détails sur la fonction `addChild($title, $params)`
        - $title : nom de l'entrée menu à afficher (éligible aux traductions Symfony2)
        - $params : array de configuration du menu:
            - pour un menu / sous menu (avec route d'action)
                - icon : extension classname glyphicon (ex : user, list, etc)
                - route : route déclarée dans le routing Symfony2
            - pour un dropdown (conteneur de sous menus)
                - icon : extension classname glyphicon (ex : user, list, etc)
                - dropdown : true||false (pour activer dropdown)
                - caret : true||false (pour afficher caret)

    - Il est donc possible de créer une logique métier complexe pour vos menus
    - le listener de création d'entrée menu étant un service, vous pouvez utiliser les **injections de dépendances** pour utiliser ce dont vous avez besoin
    - Le tout sans créer de dépendance inutile entre bundle applicatifs
    
Gestion automatisée du menu Administrateur
----------------------------------------------------

- FiducialCore fournit (suivant configuration) la possibilité de mutualiser une section de menu réservée aux administrateurs
- Cette notion de section menu Administrateur est configurable au niveau FiducialCore:

```
fiducial_core:
    menu:
        enable_admin_menu: true
        admin_menu_name: Administration
```

- Pour s'y greffer depuis vos bundle, il vous faut faire comme suit:

```
<?php
// Fia\WorkorderBundle\EventListener\ConfigureMenuListener.php
namespace Fia\WorkorderBundle\EventListener;

use Fiducial\CoreBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuListener
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @param TokenStorage $tokenStorage
     */
    public function __construct(TokenStorage $tokenStorage, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param ConfigureMenuEvent $event
     */
    public function onMenuConfigure(ConfigureMenuEvent $event)
    {
        //gets user with service container
        $user = $this->tokenStorage->getToken()->getUser();
        
        //gets menu factory with event
        $menu = $event->getMenu();
        
        //check if user authenticated
        if (is_object($user)) {
            //checks if the user is admin
            if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                //check if admin menu enabled
                if ($event->isAdminMenuEnabled()) {
                    //fetch admin menu section
                    $admin = $menu->getChild($event->getAdminMenuName());
                    //manage workorder document statuses
                    $admin->addChild('Manage Workorder Types', array('icon' => 'tags', 'route' => 'workordertype_list'));
                    $admin->addChild('divider_5', array('divider' => true));
                    $admin->addChild('Manage Workorder Documents', array('icon' => 'file', 'route' => 'document_list'));
                    $admin->addChild('Manage Workorder Document Types', array('icon' => 'tags', 'route' => 'documenttype_list'));
                }
            }
        }
    }
}
```

- Les fonctions notables sont:
    - `$event->isAdminMenuEnabled()` : teste si le menu Administrateur est activé dans la configuration FiducialCore **(param = enable_admin_menu)**
    - `$admin = $menu->getChild($event->getAdminMenuName());`: permet de récupérér la section de menu Administrateur pour y ajouter ses entrées menu


Reorganiser les entrées menu
----------------------------------------------------

- Les entrées menu étant créées depuis des *kernel.event_listener* Symfony2, il est possible de réorganiser l'ordre des entrées d'un menu / sous menu en utilisant la mécanique native de priorisation des event.
- Pour cela:
    - il faut jouer avec la valeur **priority** dans le definition tag du service: plus la valeur donnée est haute, plus elle sera traitée en premier par le kernel
    - exemple : une menu B du bundle MyB doit afficher ses entrées **avant** un menu A du bundle MyA
    
    ```
    # fichier services.yml du bundle MyA\Bundle
    services:
        menuA.configure_menu_listener:
            class: MyA\Bundle\EventListener\ConfigureMenuListener
            arguments: [ @security.token_storage, @security.authorization_checker ]
            tags:
              - { name: kernel.event_listener, event: fiducial_core.menu_configure, method: onMenuConfigure, priority: 1 }
              
    ```
    
    ```
    # fichier services.yml du bundle MyB\Bundle
    services:
        menuB.configure_menu_listener:
            class: MyB\Bundle\EventListener\ConfigureMenuListener
            arguments: [ @security.token_storage, @security.authorization_checker ]
            tags:
              - { name: kernel.event_listener, event: fiducial_core.menu_configure, method: onMenuConfigure, priority: 2 }
    ```
    
    - on voit ici que même si ces entrées menu sont rajoutées depuis 2 bundles différents, il est possible de les organiser avec **priority**
        - *B s'affichera avant A car priority de B =  2 > priority de A = 1*