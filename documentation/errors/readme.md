Pages d'erreur FiducialCore
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [Configurer l'email administrateur pour le rapport d'erreurs](#toc_3)
- [Surcharger les templates d'erreur](#toc_4)

Présentation:
-------------

- Le FiducialCore est livré avec des pages d'erreur (pour le mode **production**) pour les codes d'erreur suivants:
    - 403 : Forbidden
    - 404 : Not found
    - 405 : Unauthorized
    - 500 : Server error


Configurer l'email administrateur pour le rapport d'erreurs:
----------------------------------------------------

- Si vous le souhaitez, vous pouvez configurer FiducialCore pour proposer un lien type `mailto:` sur la page d'erreurs 500.
- Pour ce faire, renseigner dans **app/config/parameters.yml** le paramètre **brand_admin_email**:
    
    ```
    # app/config/parameters.yml
    parameters:
        brand_admin_email: responsable@myapp.com
        
    ```
    
- Pour désactiver la propulsion de ce mailto:

    ```
    # app/config/parameters.yml
    parameters:
        brand_admin_email: ~
        
    ```
    
Surcharger les templates d'erreur:
----------------------------------------------------

- FiducialCore respecte le *best practice* Symfony2 [décrit ici](http://symfony.com/fr/doc/current/cookbook/controller/error_pages.html). 
- Les templates, si vous souhaitez les surcharger, sont situées dans:
    - app\Resources\TwigBundle\views\Exception\
- Chaque template correspond à un code d'erreur:
    - exemple : **Not Found => error404.html.twig**
- Pour supporter de nouveaux codes d'erreur, il suffit de créer la template correspondante
    - exemple : **code erreur XXX => errorXXX.html.twig**
