Services Symfony2 FiducialCore : Définition des services disponibles
======

Sommaire:
-------------

- [Définition des services](#toc_2)
- [Fichiers de définition](#toc_3)
- [Récupérer la configuration du FiducialCore dans les fichiers de définition des services](#toc_4)

Définition des services:
------------------------

- Les services du FiducialCore sont définis dans le repertoire **Resources/config** du bundle FiducialCore
- Celui-ci est découpé en sous répertoires, comme suit:
    - **Config** : contient les paramètres (parameters.yml) issus de la configuration du FiducialCore (pour utilisation dans les injections de dépendance)
    - **Routing** : contient les information de routing du FiducialCore
    - **Services** : contient la liste des services offerts par le FiducialCore, classé par types dans leurs fichiers respectifs
- Le chargement customisé des services est défini dans le fichier de configuration d'injection de dépendances du bundle FiducialCore:

```
<?php 
# src/Fiducial/CoreBundle/DependencyInjection/FiducialCoreExtension.php

namespace Fiducial\CoreBundle\DependencyInjection;

class FiducialCoreExtension extends Extension
{

    ...

    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
    
        ...
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        //load common parameters
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config/Config'));
        $loader->load('parameters.yml');

        //load split configuration
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config/Services'));
        $loader->load('core.yml');
        $loader->load('doctrine.yml');
        $loader->load('events.yml');
        $loader->load('form.yml');
        $loader->load('menu.yml');
        $loader->load('twig.yml');
        $loader->load('grid.yml');
        $loader->load('utils.yml');
        $loader->load('rest.yml');
        ...
    }
    
    ...
}
```

Fichiers de définition:
------------------------

- Les définitions services du FiducialCore sont réparties dans plusieurs fichiers (pour facilité de lecture et maintenance), comme suit:
    - **core.yml** : services techniques du core (driver d'annotations, recherche globale, etc.)
    - **doctrine.yml** : services relatifs à doctrine (entity listener resolver, uploadable listener)
    - **event.yml** : services relatifs aux events *tag: kernel.event_listener*
    - **form.yml** : services relatifs aux formTypes Symfony2 *tag: form.type*
    - **grid.yml** : services relatifs aux grilles (DataGrids) FiducialCore
    - **menu.yml** : services relatifs aux menus (main, profile) du FiducialCore
    - **twig.yml** : services extensions Twig (déclarant de nouvelles fonctions Twig) *tag: twig.extension*
    - **utils.yml** : services utilitaires divers
    - **rest.yml** : services relatifs aux capacités REST
    
Récupérer la configuration du FiducialCore dans les fichiers de définition des services:
------------------------

- Le fichier **Config/parameters.yml** contient l'ensemble de la configuration du FiducialCore sous forme de paramètres Yml

```
#
# src/Fiducial/CoreBundle/Resources/config/Config/parameters.yml
parameters:
    fiducial_core.grid_parameters:
        condensed: "%fiducial_core.grid.condensed%"
        fixed_header: "%fiducial_core.grid.fixed_header%"
        readonly: "%fiducial_core.grid.readonly%"
        show_filters: "%fiducial_core.grid.show_filters%"
        show_toggler: "%fiducial_core.grid.show_toggler%"
        filters_position: "%fiducial_core.grid.filters_position%"
        actions_style: "%fiducial_core.grid.actions_style%"
        actions_column_title: "%fiducial_core.grid.actions_column_title%"
        actions_column_size: "%fiducial_core.grid.actions_column_size%"
        standalone_template: "%fiducial_core.grid.standalone_template%"
        automated_routes: "%fiducial_core.grid.automated_routes%"
        automated_mass_actions: "%fiducial_core.grid.automated_mass_actions%"
        paging: "%fiducial_core.grid.paging%"
        operators: "%fiducial_core.grid.operators%"
        exports: "%fiducial_core.grid.exports%"
        exports_limitation: "%fiducial_core.grid.exports_limitation%"

    fiducial_core.menu_parameters:
        enable_admin_menu: "%fiducial_core.menu.enable_admin_menu%"
        admin_menu_name: "%fiducial_core.menu.admin_menu_name%"
        admin_menu_user_management: "%fiducial_core.menu.admin_menu_user_management%"
        admin_menu_notification_management: "%fiducial_core.menu.admin_menu_notification_management%"

    fiducial_core.sidebar_parameters:
        use_affix: "%fiducial_core.sidebar.use_affix%"
        template: "%fiducial_core.sidebar.template%"

    fiducial_core.users_parameters:
        enable_users_registration: "%fiducial_core.users.enable_users_registration%"
        users_registration_redirection: "%fiducial_core.users.users_registration_redirection%"
        board_template: "%fiducial_core.users.board_template%"

    fiducial_core.uploadable_parameters:
        uploadable_upload_dir: "%fiducial_core.entities.uploadable_upload_dir%"
        uploadable_preview_width: "%fiducial_core.entities.uploadable_preview_width%"
        uploadable_preview_noimage: "%fiducial_core.entities.uploadable_preview_noimage%"

    fiducial_core.search_parameters:
        enable_search: "%fiducial_core.search.enable_search%"
        enable_advanced_search: "%fiducial_core.search.enable_advanced_search%"
        search_mode: "%fiducial_core.search.search_mode%"
        board_advanced_template: "%fiducial_core.search.board_advanced_template%"
        template: "%fiducial_core.search.template%"

    fiducial_core.notification_parameters:
        enable_notification: "%fiducial_core.notification.enable_notification%"
        board_template: "%fiducial_core.notification.board_template%"
        board_max_height: "%fiducial_core.notification.board_max_height%"
        list_template: "%fiducial_core.notification.list_template%"

    fiducial_core.maintenance_parameters:
        enable_maintenance: "%fiducial_core.maintenance.enable_maintenance%"
        working_back_at: "%fiducial_core.maintenance.working_back_at%"
        template: "%fiducial_core.maintenance.template%"
```

- Ceci nous permet de réutiliser ces paramètres du core dans les injections de dépendances aux déclarations de services.
- **Exemple** : déclaration d'un service qui à besoin de récupérer la configuration courante des grilles du FiducialCore

```
#src/MyBundle/Resources/config/services.yml
services:
    my_bundle.my_service:
        class: MyBundle\Services\MyServiceClass
        arguments:
            - %fiducial_core.grid_parameters%
```
