Services Symfony2 FiducialCore : Etendre/redéfinir les services disponibles
======

Sommaire:
-------------

- [Flexibilité des services FiducialCore](#toc_2)
- [Example de redéfinition de service FiducialCore](#toc_3)

Flexibilité des services FiducialCore:
------------------------

- **La totalité** des services FiducialCore ont leur classname (attribut *class*) paramètrable
- Ce paramètrage est dans chaque fichier de définition des services su core, section *parameters*
- Chaque paramètre de classe de service est nommé comme suit : **nom_du_service.class**
- Example: 

```
# src/Fiducial/CoreBundle/Resources/config/Services/grid.yml
parameters:
    #FiducialCore class definitions
    fiducial_core.grid_manager.class: Fiducial\CoreBundle\Grid\Manager
    fiducial_core.grid_entity_builder.class: Fiducial\CoreBundle\Grid\Builders\Entity\Builder

services:
    fiducial_core.grid_manager:
        class: %fiducial_core.grid_manager.class%
        scope: prototype
        arguments:
            - @grid.manager
            - %fiducial_core.grid_parameters%
    fiducial_core.grid_entity_builder:
        class: %fiducial_core.grid_entity_builder.class%
        scope: prototype
        arguments:
            - @grid
            - %fiducial_core.grid_parameters%
```

- Ceci permet au developpeur de pouvoir surcharger facilement un service FiducialCore, en:
    - redéfinissant cet attribut class dans sa propre définition de service
    - créant une classe object PHP qui étend la classe d'origine
    
Example de redéfinition de service FiducialCore:
------------------------

- Example : redéfinition du service **fiducial_core.grid_manager** du FiducialCore
- Redefinition du *paramètre de classe* du service original:

```
# src/MyBundle/Resources/config/services.yml
parameters:
    #fiducial_core.grid_manager class redefinition
    fiducial_core.grid_manager.class: MyBundle\Service\Grid\MyManager
```

- Création de la classe de redéfinition qui *étend* la classe du service original:

```
<?php 
# src/MyBundle/Service/Grid/MyManager.php

namespace MyBundle\Service\Grid;

use Fiducial\CoreBundle\Grid\Manager as FiducialCoreGridManager;

class MyManager extends FiducialCoreGridManager
{

    /**
     * Overrides getGridManager original function
     * @return GridManager
     */
    protected function getGridManager()
    {
        $gridManager = parent::getGridManager();
    
        //you custom stuff here
        ...
        
        //return
        return $gridManager;
    }

    ...

    /**
     * Adds a custom function
     * @param mixed $params : some params
     */
    public function myCustomFunction($params)
    {
        //put here your wonderful code :)
    }
    
    ...
}
```

- Ainsi, toutes les grilles propulsées par le manager de grilles FiducialCore utiliseront cette nouvelle classe object, dans laquelle votre logique sera apportée.
- Faire ainsi est le **meilleur moyen de rester compatible avec le FiducialCore**, notamment au niveau de ses montées de version.