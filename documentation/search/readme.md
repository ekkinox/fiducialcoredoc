Moteur de recherche FiducialCore
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [Recherche avancée](#toc_3)
- [Rendre une entité éligible au moteur de recherche](#toc_4)

Présentation:
-------------

- Le FiducialCore est livré avec un moteur de recherche, basé sur des requêteurs Doctrine (queryBuilders).
- Il peut rechercher sur des entités doctrine qui ne sont pas référencées sur la connexion doctrine par défaut (auto-détermination du manager name)
- Le moteur de recherche (par défaut désactivé) est configurable au niveau FiducialCore, comme suit:

```
# configuration FiducialCore, avec ici les valeurs par défaut
fiducial_core:
    # ...
    search:
        #activation de la recherche
        enable_search: false
        #activation de la recherche avancée
        enable_advanced_search: false
        #mode de recherche, valeurs possibles: 'static', 'ajax'
        search_mode: 'static'
        #template utilisée pour le formulaire de recherche
        template: 'FiducialCoreBundle:Search:form.html.twig'
        #template de la card dashboard pour formulaire de recherche avancée
        board_advanced_template: 'FiducialCoreBundle:Search:Board/advanced.html.twig'
```

- Controller et actions utilisés :
    - **FiducialCoreBundle:Core:searchAction** : recherche standard
    - **FiducialCoreBundle:Core:advancedSearchAction** : recherche avancée
- Services impliqués:
    - **Fiducial\CoreBundle\Service\Annotation\Driver** : utilisé pour parsing des annotation FiducialCore @Searchable, entre autres
    - **Fiducial\CoreBundle\Service\Search\Manager** : constructeur de querybuilders dynamiques pour effectuer la recherche, entre autres


Recherche avancée:
-------------

- FiducialCore est livré avec un **mode avancé** pour son moteur de recherche
    - activable via ```fiducial_core.search.enable_advanced_search = true```
- Le mode avancé permet:
    - de pouvoir choisir sur quelles entités effectuer la recherche
        - le driver d'annotations ne selectionnera que les entités choisies dans la liste des entités annotées par @Searchable
    - d'effectuer une recherche sur une date, transformant le champ de recherche en datepicker
    - la selection des options de recherche est stockée en session utilisateur, rendant son utilisation plus intuitive
    
- Une extension Twig est fournie pour automatiquement construire le formulaire de recherche avancée, via cette fonction Twig:

    ```
    {{ fiducialcore_generate_advanced_search_board() }}
    ```


Rendre une entité éligible au moteur de recherche:
----------------------------------------------------

- FiducialCore utilise une annotation personalisée **@Searchable** (et gérée par son propre driver d'annotations) afin de rendre **très facilement** éligible une entité à la recherche globale
- Pour cela, il vous faut:
    - annoter votre entité avec l'annotation FiducialCore **@Searchable**
        - exemple:
        
        ```
        /**
         * @Searchable(fields={"*"})
         */
        ```
        
        - fields : liste des attributs de l'entité sur laquelle la recherche va s'opérer
            - **si fields={}**, recherchera sur l'id de l'entité
            - **si fields={"*"}**, recherchera sur tous les champs de l'entité qui ne sont pas un mapping vers une autre entité
            - **si fields={"field1","field2"}**, recherchera sur les champs listés dans cet array
        
    - implémenter l'interface FiducialCore **ListableEntityInterface** sur votre entité, afin d'implémenter les méthodes necessaires à l'entité pour etre rendue dans les resultats de recherche (listable)
        - exemple:
        
        ```
        <?php
        use Fiducial\CoreBundle\Entity\Interfaces\ListableEntityInterface;
        
        class BusinessUnit implements ListableEntityInterface
        {
        
            //...
        
        }
        ```
        
- Pour résumer, voici un exemple: l'entité *FiaRepositoryBundle:BusinessUnit* sera recherchable sur tout ses attributs non mappés depuis la recherche globale:
    
    ```
    <?php
    
    namespace Fia\RepositoryBundle\Entity;
    
    use Doctrine\ORM\Mapping as ORM;
    use Fiducial\CoreBundle\Annotations\Entity as CoreEntity;
    use Fiducial\CoreBundle\Entity\Interfaces\ListableEntityInterface;
    
    
    /**
     * BusinessUnit.
     *
     * @ORM\Table()
     * @ORM\Entity(repositoryClass="Fia\RepositoryBundle\Entity\BusinessUnitRepository")
     * @CoreEntity\Searchable(fields={"*"})
     */
    class BusinessUnit implements ListableEntityInterface
    {
        
        //...
        
        /**
         * @return string
         */
        public function renderAsListableEntity()
        {
            return '<strong>'.$this->getName().'</strong>&nbsp;(&nbsp;'.$this->getCode().'&nbsp;)';
        }
    
        /**
         * @return array
         */
        public function getListableEntityActions()
        {
            return array(
                'see' => array(
                    'route' => $this->getEntityRoutePrefix() . '_see',
                    'params' => array('id' => $this->getId()),
                    'color' => 'info',
                    'icon' => 'search',
                )
            );
        }
    
        /**
         * @return string
         */
        public static function getEntityName()
        {
            return 'businessunit';
        }
    
        /**
         * @return string
         */
        public static function getEntityIconClass()
        {
            return 'home';
        }
    
        /**
         * @return string
         */
        public static function getEntityRoutePrefix()
        {
            return 'businessunit';
        }
            
        //...
    }
    
    ```
        