FiducialCore : Instructions d'installation
======

Installation (CentOS RHEL 5 ou 6)
--------------------------------

- Installer Composer ([procédure](https://getcomposer.org/download/))

- Installer NPM (NodeJS Package Manager)

    ```
    $ sudo yum install nodejs npm
    ```
    
- Installer less (CSS Compilator) v1.7.5 (scope global)

    ```
    $ npm install -g less@1.7.5
    ```

- Installer phantomJs (scope global)

    ```
    $ npm install -g phantomjs
    ```

- Executer composer install (pour récupération des vendors necessaires)

    ```
    $ composer install
    ```
    
- Créer un schéma de base de données (via Doctrine)

    ```
    $ php app/console doctrine:database:create
    ```

- Activer MopaBootstrap less symlink

    ```
    $ php app/console mopa:bootstrap:symlink:less
    ```

- Installer les assets avec assetic

    ```
    $ php app/console assets:install
    ```

- Dump des ressources satiques via Assetic

    ```
    $ php app/console assetic:dump
    ```

- Persmissions système

    ```
    $ HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
    $ sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
    $ sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX app/cache app/logs
    ```

- Clear du cache

    ```
    $ php app/console cache:clear
    ```
    
- Installation de phantomjs (manuellement):

    ```
    $ wget  https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-1.9.8-linux-x86_64.tar.bz2 
    $ sudo -s
    $ tar xvjf phantomjs-1.9.8-linux-x86_64.tar.bz2
    $ mv phantomjs-1.9.8-linux-x86_64 /usr/local/share
    $ ln -sf /usr/local/share/phantomjs-1.9.8-linux-x86_64/bin/phantomjs /usr/local/share/phantomjs
    $ ln -sf /usr/local/share/phantomjs-1.9.8-linux-x86_64/bin/phantomjs /usr/local/bin/phantomjs
    $ ln -sf /usr/local/share/phantomjs-1.9.8-linux-x86_64/bin/phantomjs /usr/bin/phantomjs
    ```
    
- Pour tester le fonctionnement : 

    ```
    $ phantomjs --webdriver=8643
    ```
    
- Enjoy ! 
   