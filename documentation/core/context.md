Contexte Object PHP autour des entités FiducialCore
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [Controllers](#toc_3)
- [Interfaces](#toc_4)
- [Listeners](#toc_5)
- [Repositories](#toc_6)
- [Traits](#toc_7)
    

Présentation:
-------------

- Le FiducialCore est livré avec un contexte object PHP capitalisé pour accélerer / automatiser certains process de developpement.
- Ce contexte est disponible sous le repertoire **Entity**, et s'articule comme suit:

```
*CoreBundle*
|_*Entity* : contexte object PHP autour des entités Doctrine FiducialCore
|    |_*Controllers* : controlleurs custom pour entités
|    |_*Interfaces* : interfaces custom pour entités
|    |_*Listeners* : listeners custom pour entités
|    |_*Repositories* : repositories custom pour entités
|    |_*Traits* : traits custom pour entités
```

- Cette captitalisation de contexte object PHP permet de fournir au developpeur un pool de code technique / métier prêt à l'emploi.

Controllers
---------------------

- **CrudController** : abstract class pour controllers Crud
    - factorisation de code dans une abstract class pour les Cruds générés par `$ php app/console fiducialcore:generate:crud` sans demande de compatibilité REST
    - utilisation du trait PHP **CrudControllerTrait** (Fiducial\CoreBundle\Entity\Controllers\Traits\CrudControllerTrait.php)
    - automatise la gestion Doctrine (par implémentation de l'interface **CrudControllerInterface**):
        - centralise le nom du manager Doctrine utilisé (getEntityManagerName()), utile pour bundles sur connexions non défault
        - centralise le nom de l'entity repository Doctrine utilisé (getEntityRepositoryName())
    - automatise la gestion du label de l'entité (pour vues twig) via getEntityLabel() et getEntityPluralLabel()
    - propose quelques fonctions interessantes:
        - getAllEntities() : récupération de toutes les entités du Crud
        - getEntities() : récupération des entités du Crud avec critères de recherche
        - getEntity() : récupération d'une entité du Crud avec critères de recherche
        - processXXX() : fonction de processing des actions du Crud (exemple : processForm, procesView, processGrid, etc.)
    - **Note** : pour pouvoir utiliser cette abstract class, il faut implémenter à minima les méthodes suivantes de l'interface **CrudControllerInterface** (les autre méthodes étant déja implémentées avec un comportement par défaut dans le trait CrudControllerTrait):
        - getEntityName
        - getEntityRepositoryName
    - toutes les methodes de l'abstract class CrudController ou du trait CrudControllerTrait peuvent être surchargées (scopes public et protected)
    - exemple d'implémentation (issu d'un générateur de Cruds FiducialCore):
    
    ```
    <?php
    
    namespace Fia\RepositoryBundle\Controller;
    
    use Symfony\Component\HttpFoundation\Request;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
    use Fiducial\CoreBundle\Entity\Controllers\CrudController;
    
    use Fia\RepositoryBundle\Entity\Article;
    use Fia\RepositoryBundle\Form\ArticleType;
    
    /**
     * Article controller.
     * Generated with FiducialCore Crud Generator
     * @Security("has_role('ROLE_MANAGER')")
     */
    class ArticleController extends CrudController
    {
    
        /**
         * @inheritdoc
         */
        public function getEntityName()
        {
            return 'article';
        }
    
        /**
         * @inheritdoc
         */
        public function getEntityRepositoryName()
        {
            return 'FiaRepositoryBundle:Article';
        }
    
        /**
         * @Route("/article", name="article_index")
         * @Method("GET")
         *
         * Index action
         * @param Request $request
         * @return \Symfony\Component\HttpFoundation\RedirectResponse
         */
        public function indexAction(Request $request)
        {
            return $this->processRedirectToGrid();
        }
    
        /**
         * @Route("/article/list", name="article_list")
         *
         * Lists all Article entities using Grid
         * @param Request $request
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function listAction(Request $request)
        {
            return $this->processGrid($request);
        }
    
        /**
         * @Route("/article/see/{id}", name="article_see")
         * @Method("GET")
         *
         * Finds and displays a Article entity.
         * @param Request $request
         * @param Article $entity
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function seeAction(Request $request, Article $entity)
        {
            return $this->processView($request, $entity);
        }
    
        /**
         * @Route("/article/add", name="article_add")
         *
         * Displays a form to add a new Article entity.
         * @param Request $request
         * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
         */
        public function addAction(Request $request)
        {
            return $this->processForm(
                $request,
                new ArticleType(),
                new Article()
            );
        }
    
        /**
         * @Route("/article/edit/{id}", name="article_edit")
         *
         * Displays a form to edit an existing Article entity.
         * @param Request $request
         * @param Article $entity
         * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
         */
        public function editAction(Request $request, Article $entity)
        {
            return $this->processForm(
                $request,
                new ArticleType(),
                $entity,
                self::ACTION_EDIT
            );
        }
    
        /**
         * @Route("/article/delete/{id}", name="article_delete")
         *
         * Deletes a Article entity.
         * @param Request $request
         * @param Article $entity
         * @return \Symfony\Component\HttpFoundation\RedirectResponse
         */
        public function deleteAction(Request $request, Article $entity)
        {
            return $this->processDelete($request, $entity);
        }
    
    }

    ```
- **CrudRESTController** : abstract class pour controllers Crud compatibles Webservices REST
    - factorisation de code dans une abstract class pour les Cruds générés par `$ php app/console fiducialcore:generate:crud` avec demande de compatibilité REST
    - implémentation de **CrudControllerInterface** et utilisation du trait **CrudControllerTrait** identiques au controller abstrait **CrudController**
    - ajoute les méthodes supplémentaires suivantes (pour support de REST):
        - getAction($id) : pour *GET* REST
        - cgetAction(Request $request) : pour *CGET* REST

Interfaces
---------------------

- **CoreEntityInterface** : interface pour class d'entité générique FiducialCore
    - utilisée pour cadrer l'implémentation dans les Cruds FiducialCore
    - implique l'implémentation des méthodes suivantes:
        - getEntityName() : nom de l'entité (pour construction des clefs de traduction)
        - getEntityIconClass() : portion de classname CSS glyphicon pour remplir le pattern XXX dans `<span class="glyphicon glyphicon-XXX"></span>`
        - getEntityRoutePrefix() : pour avoir le préfix des routes du Crud (exemple XXX_see, XXX_delete, etc.)
    - une entité générée par `php app/console fiducialcore:generate:entity` implémentera automatiquement cette interface
    - exemple d'implémentation:
    
    ```
    <?php
    
    namespace Fia\RepositoryBundle\Entity;
    
    use Doctrine\ORM\Mapping as ORM;
    use Fiducial\CoreBundle\Entity\Interfaces\CoreEntityInterface;
    
    /**
     * Article.
     *
     * @ORM\Table()
     * @ORM\Entity(repositoryClass="Fia\RepositoryBundle\Entity\ArticleRepository")
     */
    class Article implements CoreEntityInterface
    {
        //...
    
        /**
         * @return string
         */
        public static function getEntityName()
        {
            return 'article';
        }
    
        /**
         * @return string
         */
        public static function getEntityIconClass()
        {
            return 'list';
        }
    
        /**
         * @return string
         */
        public static function getEntityRoutePrefix()
        {
            return 'article';
        }
        
        //...
        
    }
    ```

- **ListableEntityInterface** : interface pour class d'entité listable
    - extends l'interface *CoreEntityInterface* : donc si une entité implémente ListableEntityInterface, **elle doit donc aussi implémenter les méthodes de l'interface CoreEntityInterface**
    - utilisée pour cadrer le rendu d'une entité dans les résultat de recherche du moteur de recherche FiducialCore
    - utilisée pour cadrer le rendu d'une entité dans l'appel de la fonction Twig FiducialCore ` {{ fiducialcore_render_related_entities(entity, withActions) }}`
        - entity : entité dont on doit lister les relations Doctrine
        - withActions : boolean pour afficher ou non les actions des entités listées rendues par la fonction getListableRelatedEntityActions()
    - implique l'implémentation des méthodes suivantes:
        - renderAsListableEntity() : rendu HTML pour entrée de liste dans la liste des entités relatives
        - getListableEntityActions() : liste des actions pour entrées de liste dans la liste des entités relatives
        - getEntityName() : nom de l'entité (pour construction des clefs de traduction)
        - getEntityIconClass() : portion de classname CSS glyphicon pour remplir le pattern XXX dans `<span class="glyphicon glyphicon-XXX"></span>`
        - getEntityRoutePrefix() : pour avoir le préfix des routes du Crud (exemple XXX_see, XXX_delete, etc.)
    - une entité générée par `php app/console fiducialcore:generate:entity` avec la compatibilité **Searchable** implémentera automatiquement cette interface
    - exemple d'implémentation:
    
    ```
    <?php
    
    namespace Fia\RepositoryBundle\Entity;
    
    use Doctrine\ORM\Mapping as ORM;
    use Fiducial\CoreBundle\Entity\Interfaces\ListableEntityInterface;
    
    /**
     * Article.
     *
     * @ORM\Table()
     * @ORM\Entity(repositoryClass="Fia\RepositoryBundle\Entity\ArticleRepository")
     */
    class Article implements ListableEntityInterface
    {
        //...
    
        /**
         * @return string
         */
        public function renderAsListableEntity()
        {
            return '<strong>'.$this->getLabel().'</strong>&nbsp;(&nbsp;code:&nbsp;'.$this->getCode().'&nbsp;)';
        }
    
        /**
         * @return array
         */
        public function getListableEntityActions()
        {
            return array(
                'see' => array(
                    'route' => $this->getEntityRoutePrefix().'_see',
                    'params' => array('id' => $this->getId()),
                    'color' => 'info',
                    'icon' => 'search',
                ),
            );
        }
    
        /**
         * @return string
         */
        public static function getEntityName()
        {
            return 'article';
        }
    
        /**
         * @return string
         */
        public static function getEntityIconClass()
        {
            return 'list';
        }
    
        /**
         * @return string
         */
        public static function getEntityRoutePrefix()
        {
            return 'article';
        }
        
        //...
        
    }
    ```

Listeners
---------------------

- **UploadableConfigurationListener** : entity listener doctrine résolu par *FiducialCore EntityListeners Resolver*
    - utilisée pour le type d'entité FiducialCore **Uploadable**, pour automatiser la lecture des paramètres FiducialCore (emplacement d'upload, etc.)
    - doit étre invoqué via l'annotation: `@ORM\EntityListeners({ "Fiducial\CoreBundle\Entity\Listeners\UploadableConfigurationListener" })`
    - exemple d'implémentation:
    
    ```
    <?php
    
    namespace Fia\WorkorderBundle\Entity;
    
    use Doctrine\ORM\Mapping as ORM;
 
    /**
     * Document.
     *
     * @ORM\Table()
     * @ORM\Entity(repositoryClass="Fia\WorkorderBundle\Entity\DocumentRepository")
     * @ORM\EntityListeners({ "Fiducial\CoreBundle\Entity\Listeners\UploadableConfigurationListener" })
     * @ORM\HasLifecycleCallbacks
     */
    class Document
    {
    
        //...
        
    }
    ```


Repositories
---------------------

- **CoreRepository** : abstract class entity repository automatisant la récupération de la liste d'entités et l'utilisation du tableAlias dans les queryBuilders
    - implémente l'interface **CoreRepositoryInterface**, qui implique l'implémentation de:
        - getCoreTableAlias() : recupération du tableAlias pour les queryBuilders
    - exemple d'implémentation:
    
    ```
    <?php
    
    namespace Fia\RepositoryBundle\Entity;
    
    use Fiducial\CoreBundle\Entity\Repositories\Model\CoreRepository;
    
    /**
     * LogoRepository
     *
     * This class was generated by the Doctrine ORM. Add your own custom
     * repository methods below.
     */
    class LogoRepository extends CoreRepository
    {
        /**
         * @return string
         */
        public function getCoreTableAlias()
        {
            return 'l';
        }
        
        //your code here ...
    }
    ```
    
- **SoftDeletableRepository** : abstract class entity repository automatisant la gestion des opérations Doctrine sur les entités de type *SoftDeletable*
    - permet l'automatisation des filtrages sur les entités actives ou supprimées logiquement
    - extends l'abstract class **CoreRepository**, et donc implémente l'interface **CoreRepositoryInterface**
    - automatise la gestion des formTypes FiducialCore **fiducial_core_soft_deletable_entity** (mais peut aussi servir pour utilisation non interne FiducialCore)
        - addArchivedFilter(): ajoute filtrage archivé / actif pour les queryBuilders
        - getActiveEntitiesListForFormType(): prépare une liste d'entités actives pour un formType fiducial_core_soft_deletable_entity
    - exemple d'implémentation:
    
    ```
    <?php
    
    namespace Fia\RepositoryBundle\Entity;
    
    use Fiducial\CoreBundle\Entity\Repositories\SoftDeletableRepository;
    
    /**
     * ArticlePriceRepository.
     *
     * This class was generated by the Doctrine ORM. Add your own custom
     * repository methods below.
     */
    class ArticlePriceRepository extends SoftDeletableRepository
    {
        /**
         * @return string
         */
        public function getCoreTableAlias()
        {
            return 'ap';
        }
        
        //your code here ...
    }
    ```


 Traits
---------------------


- **TimestampableTrait**: trait PHP permettant l'automatisation des sauvegardes des dates de création et modification d'une entité.
    - utilisé par le type d'entité FiducialCore **Timestampable**
    - vous trouverez [plus de détails ici](entity.md#toc_4)
    - exemple d'implémentation:
    
    ```
    <?php
    namespace Fia\RepositoryBundle\Entity;
    
    use Doctrine\ORM\Mapping as ORM;
    use Fiducial\CoreBundle\Entity\Traits\TimestampableTrait;
    
    /**
     * Article.
     *
     * @ORM\Table()
     * @ORM\Entity(repositoryClass="Fia\RepositoryBundle\Entity\ArticleRepository")
     * @ORM\HasLifecycleCallbacks
     */
    class Article
    {
        /**
         * handle timestamp markers eligibility
         * @see TimestampableTrait
         */
        use TimestampableTrait;
        
        //...
        
    }
    ```
    
- **SoftDeletableTrait**: trait PHP permettant l'automatisation de la suppression logique / activation d'entités
    - utilisé par le type d'entité FiducialCore **SoftDeletable**
    - vous trouverez [plus de détails ici](entity.md#toc_5)
    - exemple d'implémentation:
    
    ```
    <?php
    namespace Fia\RepositoryBundle\Entity;
    
    use Doctrine\ORM\Mapping as ORM;
    use Fiducial\CoreBundle\Entity\Traits\SoftDeletableTrait;
    
    /**
     * Article.
     *
     * @ORM\Table()
     * @ORM\Entity(repositoryClass="Fia\RepositoryBundle\Entity\ArticleRepository")
     * @ORM\HasLifecycleCallbacks
     */
    class Article
    {
        /**
         * handle soft deletion eligibility
         */
        use SoftDeletableTrait;
        
        //...
        
    }
    ```
    
- **UploadableTrait**: trait PHP permettant l'automatisation de l'upload/download de ressources fichier liées à une entité
    - utilisé par le type d'entité FiducialCore **Uploadable**
    - vous trouverez [plus de détails ici](entity.md#toc_6)
    - exemple d'implémentation:
    
    ```
    <?php
    
    namespace Fia\WorkorderBundle\Entity;
    
    use Doctrine\ORM\Mapping as ORM;
    use Fiducial\CoreBundle\Entity\Traits\UploadableTrait;
    
    /**
     * Document.
     *
     * @ORM\Table()
     * @ORM\Entity(repositoryClass="Fia\WorkorderBundle\Entity\DocumentRepository")
     * @ORM\EntityListeners({ "Fiducial\CoreBundle\Entity\Listeners\UploadableConfigurationListener" })
     * @ORM\HasLifecycleCallbacks
     */
    class Document
    {
        /**
         * Handle uploadable eligibility
         *
         * @see UploadableTrait
         */
        use UploadableTrait;
        
        //...
        
    }
    ```
    