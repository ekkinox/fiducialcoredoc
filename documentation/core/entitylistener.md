Utilisation du Resolver FiducialCore pour implémentation d'entity listeners doctrine
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [Exemple d'utilisation](#toc_3)
    

Présentation:
-------------

- La programmation object par évenements est de nos jours une necessité. Non discutable.
- Depuis Doctrine 2.4, il est possible de poser des entity listeners, ce qui est interessant car:
    - les **lifeCycleCallback** Doctrine sont propres au code de l'entité (on ne peut pas injecter de dépendances pour nos traitements évenementiels)
    - les **EventListeners** Doctrine répondent à cette problématique d'injection de dépendances, mais toutes les entités d'un manager passent par ces listeners, ce qui est problématique niveau performances
    - les **EntityListeners** Doctrine permettent de faire comme les EventListeners, mais seulement sur des entités ciblées
    - ....mais ceci n'est toujours pas satisfaisant, car c'est Doctrine qui s'occupe de la construction de ces listeners, empechant donc l'injection de dépendances
- FiducialCore est donc livré avec un résolveur permattant d'implémenter la mécanique EntityListener, mais exposé en tant que service, ce qui autorise l'injection de dépendances.

```
#Fiducial\CoreBundle\Resources\config\Services\doctrine.yml
parameters:
    fiducial_core.doctrine.entity_listener_resolver.class: Fiducial\CoreBundle\Doctrine\EntityListener\Resolver
    #...

services:
    fiducial_core.doctrine.entity_listener_resolver:
        class: %fiducial_core.doctrine.entity_listener_resolver.class%
        arguments: [ @service_container ]

    #...
```

- FiducialCore s'occupe de la résolution pour tout le kernel Symfony2 (par [passe de complilation](http://symfony.com/fr/doc/current/cookbook/service_container/compiler_passes.html)), ainsi même vos bundles applicatifs peuvent profiter de cette mécanique EntityListeners

```
<?php

namespace Fiducial\CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class DoctrineEntityListenerPass
 * @package Fiducial\CoreBundle\DependencyInjection\Compiler
 */
class DoctrineEntityListenerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('fiducial_core.doctrine.entity_listener_resolver');
        $services = $container->findTaggedServiceIds('doctrine.entity_listener');
        //addMethodCall
        foreach ($services as $service => $attributes) {
            $definition->addMethodCall(
                'addMapping',
                array($container->getDefinition($service)->getClass(), $service)
            );
        }
    }
}
```

- On voit ici que pour les services taggés **doctrine.entity_listener**, le resolveur (service fiducial_core.doctrine.entity_listener_resolver) FiducialCore sera invoqué
- Pour pouvoir utiliser cette mécanique, il vous faut renseigner dans votre configuration doctrine que l'entity_listener_resolver sera désormais celui de FiducialCore:
    
```
entity_listener_resolver: fiducial_core.doctrine.entity_listener_resolver
```
    
- Par exemple: 

```
#app/config/config.yml

#...

# Doctrine Configuration
doctrine:
    dbal:
        default_connection: fia
        types:
            rowid: Fiducial\CoreBundle\Doctrine\DBAL\Types\Rowid
        connections:
            fia:
                driver:   "%database_driver%"
                host:     "%database_host%"
                port:     "%database_port%"
                dbname:   "%database_name%"
                user:     "%database_user%"
                password: "%database_password%"
                charset:  UTF8
    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        default_entity_manager: fia
        entity_managers:
            fia:
                entity_listener_resolver: fiducial_core.doctrine.entity_listener_resolver
                connection: fia
                mappings:
                    FiducialCoreBundle : ~
                    FiducialUserBundle : ~
                    FiaMainBundle : ~
                    FiaRepositoryBundle : ~
                    FiaWorkorderBundle : ~

#...

```

Exemple d'utilisation:
---------------------

- Prenons ici le listener utilisé par le type d'entité FiducialCore **Uploadable** qui permet de renseigner les paramètres FiducialCore au niveau de la gestion de l'évennement (injection de dépendance):
- Déclaration du listener en tant que service **fiducial_core.uploadable_configuration_listener**, avec comme tag **doctrine.entity_listener**:

```
#Fiducial\CoreBundle\Resources\config\Services\doctrine.yml
parameters:
    fiducial_core.uploadable_configuration_listener.class: Fiducial\CoreBundle\Entity\Listeners\UploadableConfigurationListener
    #...

services:
    fiducial_core.uploadable_configuration_listener:
        class: %fiducial_core.uploadable_configuration_listener.class%
        arguments: [ %kernel.root_dir%, %fiducial_core.entities.uploadable_upload_dir% ]
        tags:
            - { name: doctrine.entity_listener  }
            
    #...

```

- Classe PHP qui s'occupe de notre listener:

```
<?php

namespace Fiducial\CoreBundle\Entity\Listeners;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;

/**
 * Class UploadableConfigurationListener
 * @package Acme\DemoBundle\Entity\Listener
 */
class UploadableConfigurationListener
{

    const UPLOADABLE_TRAIT_CLASS = 'Fiducial\CoreBundle\Entity\Traits\UploadableTrait';

    /**
     * @var : root upload dir
     */
    private $uploadRootDir; //récupéré par injection de %kernel.root_dir%

    /**
     * @var : relative upload dir (from root)
     */
    private $uploadDir; //récupéré par injection de %fiducial_core.entities.uploadable_upload_dir%

    /**
     * @param EntityManager $entityManager
     * @param $uploadRootDir
     * @param $uploadDir
     */
    public function __construct($uploadRootDir, $uploadDir)
    {
        //affect given args from service
        $this->uploadRootDir = $uploadRootDir.'/../web/';
        $this->uploadDir = $uploadDir;
    }

    /**
     * @param $entity
     * @param LifecycleEventArgs $args
     */
    public function postLoad($entity, LifecycleEventArgs $args)
    {
        //affect upload dirs conf
        $this->_affectUploadConfiguration($entity, $args);
    }

    /**
     * @param $entity
     * @param LifecycleEventArgs $args
     */
    public function prePersist($entity, LifecycleEventArgs $args)
    {
        //affect upload dirs conf
        $this->_affectUploadConfiguration($entity, $args);
    }

    //other stuff ...

    /**
     * @param $entity
     * @param LifecycleEventArgs $args
     */
    private function _affectUploadConfiguration($entity, LifecycleEventArgs $args)
    {
        if($this->_useUploadableTrait($entity, $args)){
            //affect upload rootdir
            $entity->setUploadRootDir($this->uploadRootDir);
            //affetc upload dir
            $entity->setUploadDir($this->uploadDir);
        }
    }
    
    //other and other stuff again ...
}
```

- Il ne nous reste plus qu'a positionner notre EntityListener sur une de nos entités grace à l'annotation **@EntityListeners**, comme suit:

```

<?php

namespace Fia\WorkorderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Document.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Fia\WorkorderBundle\Entity\DocumentRepository")
 * @ORM\EntityListeners({ "Fiducial\CoreBundle\Entity\Listeners\UploadableConfigurationListener" })
 */
class Document implements ListableRelatedEntityInterface, GlobalSearchableEntityInterface
{

    //your entity code
    
}

```