Types d'entités FiducialCore: entités Doctrine préparées
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [Types d'entités disponibles](#toc_3)
    - [Timestampable](#toc_4): sauvegarde automatique des dates de création et modification de l'entité
    - [SoftDeletable](#toc_5): permet la suppression logique d'une entité (activation/desactivation avec date d'effet)
    - [Uploadable](#toc_6): gestion automatique des uploads/downloads d'une ressource fichier liée à une entité
    - [Searchable](#toc_7): rendre éligible une entité au moteur de recherche du core
    - [Versionable](#toc_8): à venir
    

Présentation:
-------------

- Le FiducialCore est livré avec des types d'entités préparées.
- Par type d'entité, nous entendons un contexte de code PHP qui apporte / automatise du code technique ou métier que le developpeur n'a pas à prendre en charge, rendant ainsi le code des applications plus clair, rapide à écrire et maintenable.
- Ces contextes de code PHP utilisés par ces types d'entités sont composés, suivant leur complexité:
    - de traits PHP,
    - d'interfaces PHP,
    - de classes abstraites PHP,
    - de listners Symfony2,
    - d'annotation personalisées,
    - etc.
- Cette captitalisation de types d'entités permet de fournir au developpeur un pool de code technique / métier prêt à l'emploi.
- Le cumul des types d'entités sur une entité est possible de par la conception du contexte PHP: par example, une entité peut à la fois être *Timestampable* et *SoftDeletable*
- Tout ce contexte PHP autour des entités est [présenté plus en détail ici](context.md)

Types d'entités disponibles
---------------------

#### Timestampable:
- **But: pouvoir offrir à une entité doctrine la gestion automatique de sauvegarde des dates de création et de modification par event Doctrine**
- Générable depuis le générateur de Cruds FiducialCore : **Oui**
- Fonctionnement: 
    - utilise les *lifeCycleCallbacks* Doctrine *PrePersist()* et *PreUpdate()*
    - automatise la gestion de 2 champs supplémentaires apportés par le trait *TimestampableTrait*
        - *createdAt*: date de création de l'entité (type datetime)
        - *updatedAt*: date de dernière modification de l'entité (type datetime)
    
- Implémentation:
    - Indiquer sur l'entité:
        - l'utilisation du trait *TimestampableTrait* (Fiducial\CoreBundle\Entity\Traits\TimestampableTrait)
        
        ```
        <?php
        use TimestampableTrait;
        ```
        
        - indiquer à doctrine l'utilisation des lifeCycleCallbacks
        
        ```
        <?php
        /**
         * @ORM\HasLifecycleCallbacks
         */
        ```
        
        - en résumé:
        
        ```
        <?php
        namespace Fia\RepositoryBundle\Entity;
        
        use Doctrine\ORM\Mapping as ORM;
        use Fiducial\CoreBundle\Entity\Traits\TimestampableTrait;
        
        /**
         * Article.
         *
         * @ORM\Table()
         * @ORM\Entity(repositoryClass="Fia\RepositoryBundle\Entity\ArticleRepository")
         * @ORM\HasLifecycleCallbacks
         */
        class Article
        {
            /**
             * handle timestamp markers eligibility
             * @see TimestampableTrait
             */
            use TimestampableTrait;
            
            //...
            
        }
        ```
- **Note** : pour mettre à jour votre entité niveau BDD, n'oubliez pas de lancer `$ php app/console doctrine:schema:update --dump-sql --force` qui sera en charge de créer les colonnes décrits dans le trait PHP.
        
#### SoftDeletable:
- **But: pouvoir offrir à une entité doctrine la gestion automatique de supression logique**
- Par suppression logique, on entend le positionnement d'un flag (archived) à true + stockage de la date de désactivation
- Générable depuis le générateur de Cruds FiducialCore : **Oui**
- Fonctionnement: 
    - permet de positionner un formType boolean (checkbox) dans les formulaires nommé "Archivé"
    - si au moment de persister l'entité ce flag est positionné à true, la date d'archivage est automatiquement enregistrée
    - utilise les *lifeCycleCallbacks* Doctrine *PrePersist()* et *PreUpdate()*
    - automatise la gestion de 2 champs supplémentaires apportés par le trait *TimestampableTrait*
        - *archived*: flag d'activité de l'entité (si true => inactif) (type booelan)
        - *archivedAt*: date de dernière désactivation de l'entité (type datetime)
    
- Implémentation:
    - Indiquer sur l'entité:
        - l'utilisation du trait *SoftDeletableTrait* (Fiducial\CoreBundle\Entity\Traits\SoftDeletableTrait)
        
        ```
        <?php
        use SoftDeletableTrait;
        ```
        
        - indiquer à doctrine l'utilisation des lifeCycleCallbacks
        
        ```
        <?php
        /**
         * @ORM\HasLifecycleCallbacks
         */
        ```
        
        - en résumé:
        
        ```
        <?php
        namespace Fia\RepositoryBundle\Entity;
        
        use Doctrine\ORM\Mapping as ORM;
        use Fiducial\CoreBundle\Entity\Traits\SoftDeletableTrait;
        
        /**
         * Article.
         *
         * @ORM\Table()
         * @ORM\Entity(repositoryClass="Fia\RepositoryBundle\Entity\ArticleRepository")
         * @ORM\HasLifecycleCallbacks
         */
        class Article
        {
            /**
             * handle soft deletion eligibility
             */
            use SoftDeletableTrait;
            
            //...
            
        }
        ```
- **Note** : pour mettre à jour votre entité niveau BDD, n'oubliez pas de lancer `$ php app/console doctrine:schema:update --dump-sql --force` qui sera en charge de créer les colonnes décrits dans le trait PHP.
- **FormType dédié aux liens SoftDeletable**: FiducialCore est livré avec un formType Symfony2 dédié à la gestion des liens entre entités SoftDeletable
    - en effet, une entité classique A liée à une entité SoftDeletable B peut vouloir conserver son lien vers B même si B est supprimée logiquement
    - ce formType (exposé en tant que service *fiducial_core.form.type.fiducial_core_soft_deletable_entity*) permet de gérer les modifications explixcites de ces liens ou bien leur conservation, de manière automatisée pour le developpeur
    - pour utiliser ce formType, dans un formBuilder :
    
    ```
    <?php
    // formBuilder d'une entité liée à l'entitié FiaRepositoryBundle:Article qui est SoftDeletable
    $builder->add(
        'article',
        'fiducial_core_soft_deletable_entity',
        array(
            'class' => 'Fia\RepositoryBundle\Entity\Article',
            //...
        )
    );
    ```
    
#### Uploadable:
- **But: pouvoir offrir à une entité doctrine la gestion automatique des uploads/downloads d'une ressource fichier liée**
- Par ressource fichier liée, on entend l'upload automatique + stockage des données fichier (taille, extension, etc.)
- L'automatisation par event Doctrine permet donc l'upload automatique d'un fichier à la persistence de l'entité, et la suppression automatique du fichier lié à la suppression de l'entité ( **évite ainsi l'existence de fichier liés orphelins sur le stockage configuré** )
- Tout ceci libère le développeur des tâches fastidieuses et redondantes de gestion des uploads / download de fichiers dans une application Web.
- Générable depuis le générateur de Cruds FiducialCore : **A venir**
- Fonctionnement: 
    - permet de positionner un formType **fiducial_core_file** (filechooser boostrap3) dans les formulaires nommé "Fichier" (pour l'attribute du trait *file*)
    - utilise les *lifeCycleCallbacks* Doctrine:
        - *PrePersist()*
        - *PostPersist()*
        - *PreUpdate()*
        - *PostUpdate()*
        - *PreRemove()*
        - *PostRemove()*
    - utilise **l'entity listener Doctrine FiducialCore** ([plus de détails sur le resolver ici](context.md)) sur *UploadableConfigurationListener*
        - Listener : Fiducial\CoreBundle\Entity\Listeners\UploadableConfigurationListener.php
        - Il est en charge de récupérer la configuration de l'emplacement d'upload via FiducialCore (voir options de configuration FiducialCore ci dessous)
    - automatise la gestion de 4 champs supplémentaires apportés par le trait *UploadableTrait*
        - *filename*: nom (path) du fichier lié (type string)
        - *extension*: extension du fichier lié (type string)
        - *mimeType*: type MIME du fichier lié (type string)
        - *size*: taille (octets) du fichier lié (type string)
    - apporte un attribut de type **Symfony\Component\HttpFoundation\File\UploadedFile**, nommé **file** (par lequel la manipulation du fichier lié s'effectue lors de traitements de formulaire)
- Configuration : FiducialCore permet une gestion de configuration centralisée pour ce qui est de la gestion des fichiers liés: 
    
    ```
    # configuration FiducialCore, avec ici les valeurs par défaut
    fiducial_core:
        # ...
        entities:
            #...
            #template du renderer des entités Uploadable
            uploadable_preview_template: 'FiducialCoreBundle:Entity:uploadable_preview.html.twig'
            #chemin par défaut d'upload des resources fichier des entités Uploadable (dans web/)
            uploadable_upload_dir: 'uploads/documents'
            #taille en px par défaut du prévisualisateur de fichiers des entités Uploadable
            uploadable_preview_width: '250'
            #image proposée en tant que prévisualisation lorsque la prévisualisation n'est pas supportée
            uploadable_preview_noimage: 'bundles/fiducialcore/images/preview/noimage.png'
    ```
- Implémentation:
    - Indiquer sur l'entité:
        - l'utilisation du trait *UploadableTrait* (Fiducial\CoreBundle\Entity\Traits\UploadableTrait)
        
        ```
        <?php
        use UploadableTrait;
        ```
        
        - indiquer à doctrine l'utilisation des lifeCycleCallbacks
        
        ```
        <?php
        /**
         * @ORM\HasLifecycleCallbacks
         */
        ```
        
        - indiquer l'utilisation de l'entity listener Doctrine FiducialCore **UploadableConfigurationListener** (Fiducial\CoreBundle\Entity\Listeners\UploadableConfigurationListener.php)
        
        ```
        <?php
        /**
         * @ORM\EntityListeners({ "Fiducial\CoreBundle\Entity\Listeners\UploadableConfigurationListener" })
         */
        ```
        
        - en résumé:
        
        ```
        <?php
        
        namespace Fia\WorkorderBundle\Entity;
        
        use Doctrine\ORM\Mapping as ORM;
        use Fiducial\CoreBundle\Entity\Traits\UploadableTrait;
        
        /**
         * Document.
         *
         * @ORM\Table()
         * @ORM\Entity(repositoryClass="Fia\WorkorderBundle\Entity\DocumentRepository")
         * @ORM\EntityListeners({ "Fiducial\CoreBundle\Entity\Listeners\UploadableConfigurationListener" })
         * @ORM\HasLifecycleCallbacks
         */
        class Document
        {
            /**
             * Handle uploadable eligibility
             *
             * @see UploadableTrait
             */
            use UploadableTrait;
            
            //...
            
        }
        ```
- **Note** : pour mettre à jour votre entité niveau BDD, n'oubliez pas de lancer `$ php app/console doctrine:schema:update --dump-sql --force` qui sera en charge de créer les colonnes décrites dans le trait PHP.
- **FormType dédié aux entités Uploadable**: FiducialCore est livré avec un formType Symfony2 type file pour intégration graphique boostrap 3.
    - ce formType est exposé en tant que service *fiducial_core.form.type.fiducial_core_file*
    - pour utiliser ce formType, dans un formBuilder :
    
    ```
    <?php
    // formBuilder de l'entité FiaWorkorderBundle:Document
    $builder->add(
        'file', //attribut de l'entité apporté par l'utilisation du trait UploadableTrait
        'fiducial_core_file',
        array(
            'required' => false,
            //...
        )
    );
    ```
- **Extension Twig pour rendu** : FiducialCore apporte aussi l'automatisation du rendu en mode visualisation de ce fichier lié grace a une Twig extension : *fiducial_core.twig.extension.entity_uploadable_preview*
    - **Interets** : automatise le traitement pour le developpeur, en offrant :
        - automatise le rendu des fichiers liés
        - présentation du fichier (nom, extension, taille, etc.)
        - possibilité de preview (modale boostrap3) configurable (voir au niveau FiducialCore) quand le fichier peut etre prévisualisé
        - possibilité de génération de lien de download automatique
    - Fonction Twig : `{{ fiducialcore_render_entity_uploadable_preview(entity, withDownload, onlyImage) }}`
    - Exemple d'utilisation : 
    
    ```
    {# template twig de visualisation #}
    {{ fiducialcore_render_entity_uploadable_preview(entity, true) }}
    ```
    
    
#### Searchable:

- **But: pouvoir offrir à une entité doctrine l'éligibilité au moteur de recherche FiducialCore**
- Par éligibilité, on entend que les queryBuilders dynamiques du moteur de recherche sauront comment construire la requête DQL par lecture de l'annotation @Searchable positionnée sur l'entité
- Tout ceci libère le développeur des tâches fastidieuses et redondantes de construction de requetes DQL de recherche sur des données d'entité Doctrine.
- Générable depuis le générateur de Cruds FiducialCore : **Oui**
- Fonctionnement: 
    - utilise l'annotation FiducialCore **@Searchable**
    - cette annotation est supportée par le driver d'annotations FiducialCore
    - l'annotation @Searchable se déclare comme suit : *@Searchable(fields={})*
        - attribut *fields* : déclaration des champs de l'entité sur lesquels effectuer la recherche
            - **fields={}** : recherche sur l'id de l'entité annotée
            - **fields={"*"}** : recherche sur tous les champs de l'entité qui ne sont pas des mappings vers d'autres entités
            - **fields={"field1","field2",...}** : recherche les champs de l'entité décrits dans l'attribut field de l'annotation
- Implémentation:
    - Indiquer sur l'entité:
        - l'utilisation de l'annotation *@Searchable* (dispo dans le pool des annotations type Entity du core)
        
        ```
        <?php
        use Fiducial\CoreBundle\Annotations\Entity as CoreEntity;
        ```
        
        - indiquer sur l'entité l'exposition au moteur de recherche (*ici par exemple recherche sur les champs "code" et "label"*)
        
        ```
        <?php
        /**
         * @CoreEntity\Searchable(fields={"code","label"})
         */
        ```
        
        - implémenter l'interface PHP *ListableEntityInterface* pour que l'entité sache comment être rendue dans la liste des resultats
        
        ```
        <?php
        use Fiducial\CoreBundle\Entity\Interfaces\ListableEntityInterface;
        ```
        
        - en résumé:
        
        ```
        <?php
        
        namespace Fia\RepositoryBundle\Entity;
        
        use Doctrine\ORM\Mapping as ORM;
        use Fiducial\CoreBundle\Annotations\Entity as CoreEntity;
        use Fiducial\CoreBundle\Entity\Interfaces\ListableEntityInterface;
        
        /**
         * Article.
         *
         * @ORM\Table()
         * @ORM\Entity(repositoryClass="Fia\RepositoryBundle\Entity\ArticleRepository")
         * @ORM\HasLifecycleCallbacks
         * @CoreEntity\Searchable(fields={"code","label"})
         */
        class Article implements ListableEntityInterface
        {
            
            //...
            
            /**
             * @return string
             */
            public function renderAsListableEntity()
            {
                return '<strong>'.$this->getLabel().'</strong>&nbsp;(&nbsp;code:&nbsp;'.$this->getCode().'&nbsp;)';
            }
        
            /**
             * @return array
             */
            public function getListableEntityActions()
            {
                return array(
                    'see' => array(
                        'route' => $this->getEntityRoutePrefix().'_see',
                        'params' => array('id' => $this->getId()),
                        'color' => 'info',
                        'icon' => 'search',
                    ),
                );
            }
        
            /**
             * @return string
             */
            public static function getEntityName()
            {
                return 'article';
            }
        
            /**
             * @return string
             */
            public static function getEntityIconClass()
            {
                return 'list';
            }
        
            /**
             * @return string
             */
            public static function getEntityRoutePrefix()
            {
                return 'article';
            }
            
            //...
            
        }
        ```

#### Versionable:

- A venir