Configuration : Options de configuration
======

Liste des options de configuration du FiducialCore:
------------------------------------------------------

- La configuration du FiducialCore est **automatisée** (avec des valeurs par défaut) par la mécanique Symfony2 de configuration automatique des bundles (fichier DependencyInjection/Configuration.php)
- Ceci lui permet donc d'avoir un **comportement configurable** d'applications en applications, **facilement modifiable par le développeur**.
- La configuration du FiducialCore se trouve dans **app/config/fiducial_core.yml** ( externalisée du fichier app/config/config.yml pour facilité de maintenance )
- Elle est chargée comme suit : 

```
# app/config/config.yml

imports:
    ...
    - { resource: fiducial_core.yml }
    ...
```

- Vous pouvez **obtenir la configuration courante du FiducialCore** (valeurs par défaut + vos paramètres) par utilisation de la commande console Symfony2:

```
$ php app/console config:dump-reference FiducialCoreBundle
```

- Liste des options avec leur **valeurs par défaut** et **supportées (cf commentaires)** par le FiducialCore :

```
# app/config/fiducial_core.yml

#Fiducial core settings
fiducial_core:
    #configuration globale comportementale des entités
    entities:
        #template des actions automatiques des CRUDS
        actions_template: 'FiducialCoreBundle:Entity:actions.html.twig'
        #template de l'indicateur de statut SoftDeletable des entités
        archive_status_template : 'FiducialCoreBundle:Entity:archive_status.html.twig'
        #template du renderer des entités Uploadable
        uploadable_preview_template: 'FiducialCoreBundle:Entity:uploadable_preview.html.twig'
        #chemin par défaut d'upload des resources fichier des entités Uploadable (dans web/)
        uploadable_upload_dir: 'uploads/documents'
        #taille en px par défaut du prévisualisateur de fichiers des entités Uploadable
        uploadable_preview_width: '250'
        #image proposée en tant que prévisualisation lorsque la prévisualisation n'est pas supportée
        uploadable_preview_noimage: 'bundles/fiducialcore/images/preview/noimage.png'
    #configuration globale (commune à toutes les grilles de l'application, peut etre surchargé par grille) des DataGrids     
    grid:
        #pour tables de grilles condensées
        condensed: false
        #pour fixer les titres de colonnes en haut de l'ecran lors du scroll en mode table
        fixed_header: false
        #pour activer le mode readonly des grilles (pas de liens vers add, edit, delete) 
        readonly: false
        #pour afficher ou non les filtres dans les grilles
        show_filters: true
        #pour afficher ou non le bouton de permutation d'affichage des grilles (table / liste)
        show_toggler: true
        #position des filtres, valeurs possibles: "internal", "external", "both"
        filters_position: internal
        #template du renderer standalone (utilisé pour rechargement Ajax)
        standalone_template: 'FiducialCoreBundle:Datagrid:standalone.html.twig'
        #style de rendu des icones de la colonne d'actions, valeurs possibles: 'icon', 'text', 'icon_text'
        actions_style: icon_text
        #taille de la colonne d'actions (pixels)
        actions_column_size: 100
        #titre de la colonne d'actions
        actions_column_title: Actions
        #liste des routes automatisées des actions de la colonne d'action (quand applicable)
        automated_routes: ['see', 'edit', 'delete']
        #liste des routes automatisées des actions de masse (quand applicable)
        automated_mass_actions: ['FiducialCoreBundle:Core:massActivate', 'FiducialCoreBundle:Core:massArchive', 'FiducialCoreBundle:Core:massDelete']
        #seuils de pagination
        paging: ['5', '10', '50', '100', '200']
        #liste des opérateurs disponibles (par défaut, tous)
        operators: ~
        #liste des exports offerts par les grilles (par defaut, tout ceux du vendor ApyDataGrid)
        exports: []
        #seuil (nombre de lignes retourné de la grille) de blocage des exports (par défaut = 0 (pas de limitation)) 
        exports_limitation: 0
    #configuration globale comportementale des utilisateurs 
    users:
        #activation de la possibilité de création (register) de nouveaux utilisateurs depuis l'application
        enable_users_registration: false
        #route de redirection lorsque la création est désactivée et sollicitée
        users_registration_redirection: 'fos_user_security_login'
        #Template de la card dashboard pour infos sur l'utilisateur courant
        board_template: 'FiducialCoreBundle:User:Board/index.html.twig'
    #configuration globale comportementale des Menus
    menu:
        #création automatique d'un menu admin (reservé ROLE_ADMIN)
        enable_admin_menu: true
        #nom du menu admin
        admin_menu_name: 'Administration'
        #ajoute l'entrée de gestion des utilisateurs dans le menu admin
        admin_menu_user_management: true
        #ajoute l'entrée de gestion des notifications dans le menu admin
        admin_menu_notification_management: true
    #configuration globale comportementale du moteur de recherche
    search:
        #activation de la recherche globale
        enable_search: false
        #activation de la recherche globale avancée
        enable_advanced_search: false
        #mode de recherche globale, valeurs possibles: 'static', 'ajax'
        search_mode: 'static'
        #template utilisée pour le formulaire de recherche
        template: 'FiducialCoreBundle:Search:form.html.twig'
        #template de la card dashboard pour formulaire de recherche avancée
        board_advanced_template: 'FiducialCoreBundle:Search:Board/advanced.html.twig'
    #configuration globale comportementale de la sidebar
    sidebar:
        #pour utiliser la sidebar en mode affix (fixée au scroll)
        use_affix: false
        #template utilsée pour la sidebar
        template: 'FiducialCoreBundle:Sidebar:affixable_sidebar.html.twig'
    #configuration globale comportementale des notifications
    notification:
        #active système des notifications
        enable_notification:  true
        #hauteur maximum de la card dashboard des notifications (en pixel, si = 0, pas de limites)
        board_max_height: 350
        #template de la card dashboard pour visualiser les notifications
        board_template: 'FiducialCoreBundle:Notification:Board/index.html.twig'
        #template du contenu ajax de la card dashboard pour visualiser les notifications
        list_template: 'FiducialCoreBundle:Notification:Board/notifications.html.twig'
    #configuration globale comportementale du lode maintenance
    maintenance:
        #active mode maintenance
        enable_maintenance: false
        #notification de retour au fonctionnement
        working_back_at: null
        #template du mode maintenance
        template: 'FiducialCoreBundle:Maintenance:index.html.twig'



```

- Vous pouvez refedinir dans chaque application ces paramètres à votre convenance dans le fichier app/config/fiducial_core.yml
- Example : 
    - n'autoriser que les opérateurs de grilles *'égal à'* et *'non égal à'*,
    - n'activer que l'export CSV des grilles,
    - activer la possibilité d'inscription d'utilisateurs,
    - et fixer la sidebar en mode affix (boostrap3):

```
# app/config/fiducial_core.yml

#Fiducial core settings
fiducial_core:
    grid:
        operators:
            - eq
            - neq
        exports: 
            - APY\DataGridBundle\Grid\Export\CSVExport
    users:
        enable_users_registration: true
    sidebar:
        use_affix: true
```

