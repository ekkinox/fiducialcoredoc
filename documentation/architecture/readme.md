Architecture FiducialCore
======

Sommaire:
-------------

- [FiducialCore](#toc_2): architecture bundle FiducialCore
- [FiducialUser](#toc_3): architecture bundle FiducialUser

FiducialCore:
-------------

- Ci dessous une présentation de l'arborescence du Bundle FiducialCore

```
*CoreBundle*
|_*Annotations* : repertoire des annotation personnalisées FiducialCore
|    |_*Entity* : annotations propres aux entités Doctrine
|    |_*Validator* : annotations propres aux contraintes de validations de données
|_*Command* : fichiers pour les commandes FiducialCore (fiducial:generate:crud, etc.)
|_*DependencyInjection* : standard Symfony2
|    |_*Compiler* : passes de compilations custom du FiducialCore
|_*Doctrine* : customisations Doctrine par FiducialCore
|    |_*DBAL* : customisations DBAL Doctrine
|    |_*EntityListener* : resolution de l'entity_listener customisé
|_*Entity* : contexte object PHP autour des entités Doctrine FiducialCore
|    |_*Controllers* : controlleurs custom pour entités
|    |_*Interfaces* : interfaces custom pour entités
|    |_*Listeners* : listeners custom pour entités
|    |_*Repositories* : repositories custom pour entités
|    |_*Traits* : traits custom pour entités
|_*Event* : events FiducialCore
|_*EventListener* : event listeners FiducialCore
|_*Form* : formTypes FiducialCore
|    |_*DataTransformer* :  dataTransformer pour formTypes FiducialCore
|    |_*Type* : formTypes FiducialCore
|    |_*Validator* : validateurs pour formTypes FiducialCore
|_*Generator* : classes utilisées par les générateurs FiducialCore (bundle, entity, Crud)
|_*Grid* : classes utilisées par les grilles FiducialCore
|    |_*Builders* : GridBuilders
|    |_*Engine* : surcharges vendor APYDataGrid
|    |_*Twig* : extensions Twig des grilles
|    |_*Manager.php* : GridManager
|_*Library* : librairies atomiques autoportées
|_*Menu* : menu builder FiducialCore
|_*Resources* : standard Symfony2
|_*Service* : services FiducialCore
|    |_*Annotation* : driver d'annotations personnalisées
|    |_*Fetchers* : récupérateurs de données
|    |_*Helpers* : services Helpers divers
|    |_*Inflector* : services Inflector REST
|    |_*Notification* : pour le moteur de notifications FiducialCore
|    |_*Search* : pour le moteur de recherche FiducialCore
|_*Tests* : TUA / TFA FiducialCore
|_*Twig* : extensions Twig FiducialCore
|    |_*Entities* : extensions Twig pour entités FiducialCore
|    |_*Notification* : extensions Twig pour les notifications
|    |_*Search* : extensions Twig pour le moteur de recherche
|    |_*Sidebar* : extensions Twig pour sidebar FiducialCore
|    |_*User* : extensions Twig pour les utilisateurs
|    |_*Utils* : extensions Twig diverses
|_*FiducialCoreBundle.php* : loader du bundle
```

FiducialUser:
-------------

- Ci dessous une présentation de l'arborescence du Bundle FiducialUser

```
*CoreBundle*
|_*Controller* : controlleurs de sécurité FiducialUser
|_*DependencyInjection* : standard Symfony2
|_*Entity* : entités prêtes a l'emploi (FiducialUser, FiducialCASUser)
|_*Model* : modèles (abstract classes) FiducialUser
|_*Resources* : standard Symfony2
|_*Service* : services FiducialUser
|_*Tests* : TUA / TFA FiducialUser
|_*FiducialUserBundle.php* : loader du bundle
```