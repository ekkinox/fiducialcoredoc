Grilles FiducialCore (pour entités Doctrine)
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [Configuration sur 3 niveaux](#toc_3)
- [Options de configuration disponibles](#toc_4)
- [Gestion de grilles par GridManager](#toc_5)
- [Gestion de grille par GridBuilder](#toc_6)
- [Modification de la source de grille](#toc_7)
- [Gestion des actions (row) automatisées](#toc_8)
- [Gestion des actions de masse automatisées](#toc_9)
- [Manipulation des actions (row) déclarées](#toc_10)

Présentation:
-------------

- Les dataGrids FiducialCore sont basées sur l'utilisation du bundle [APYDataGridBundle](https://github.com/Abhoryo/APYDataGridBundle)
- Le code FiducialCore autour de cette base object automatise l'utilisation de celles ci, aux niveaux:
    - de la configuration centralisée et hierarchique
    - du rendu responsive natif Bootstrap 3
    - du filtrage complexe
    - de l'automatisation des routes Symfony2 dans les colonnes d'action et massActions
    - de la cohabitation de plusieurs grilles dans la meme page
    - du rendu Ajax
    - etc.
- Les services de grilles du FiducialCore utilisent ceux du bundle APYDataGridBundle, aussi toute la [documentation technique du vendor](https://github.com/Abhoryo/APYDataGridBundle/tree/master/Resources/doc) est appliquable.

Configuration sur 3 niveaux:
------------------------

- Il existe **3 niveaux de configuration** pour les grilles FiducialCore, présentés par *ordre décroissant*:
    - **niveau 1: FiducialCore** : configuration globale pour toutes les grilles de l'application
    - **niveau 2: GridManager** : configuration partagée entre toutes les grilles d'un manager (service FiducialCore GridManager)
    - **niveau 3: GridBuilder** : configuration ciblée pour une seule grille
- Cette mécanique de configuration fonctionne par un **système de redéfinition**. En effet: 
    - il est possible de définir une configuration de grilles au niveau *FiducialCore* (pour toutes les grilles de l'application), ce sera le comportement par défaut si on ne redéfinit pas la configuration aux niveau inférieurs
    - si on redéfinit une configuration au niveau d'un *GridManager*, toutes les grilles de ce GridManager auront cette nouvelle configuration, écrasant celle du FiducialCore
    - si on redéfinit une configuration au niveau d'un *GridBuilder* (grille ciblée), même issue d'un GridManager, cette configuration ne s'appliquera qu'a cette grille ciblée, écrasant la configuration des niveaux supérieurs

Options de configuration disponibles:
------------------------

- Les options de configuration disponibles sont les suivantes (avec leur valeurs par défaut):

```
#nom de manager Doctrine, par défaut = null => prend la connexion définie par défaut dans Doctrine
manager_name: null
#pour tables de grilles condensées
condensed: false
#pour fixer les titres de colonnes en haut de l'ecran lors du scroll en mode table
fixed_header: false
#pour activer le mode readonly des grilles (pas de liens vers add, edit, delete) 
readonly: false
#pour afficher ou non les filtres dans les grilles
show_filters: true
#pour afficher ou non le bouton de permutation d'affichage des grilles (table / liste)
show_toggler: true
#position des filtres, valeurs possibles: "internal", "external", "both"
filters_position: internal
#template du renderer standalone (utilisé pour rechargement Ajax)
standalone_template: 'FiducialCoreBundle:Datagrid:standalone.html.twig'
#style de rendu des icones de la colonne d'actions, valeurs possibles: 'icon', 'text', 'icon_text'
actions_style: icon_text
#taille de la colonne d'actions (pixels)
actions_column_size: 100
#titre de la colonne d'actions
actions_column_title: Actions
#liste des routes automatisées des actions de la colonne d'action (quand applicable)
automated_routes: ['see', 'edit', 'delete']
#liste des routes automatisées des actions de masse (quand applicable)
automated_mass_actions: ['FiducialCoreBundle:Core:massActivate', 'FiducialCoreBundle:Core:massArchive', 'FiducialCoreBundle:Core:massDelete']
#seuils de pagination
paging: ['5', '10', '50', '100', '200']
#liste des opérateurs disponibles (par défaut, tous)
operators: ~
#liste des exports offerts par les grilles (par defaut, tout ceux du vendor ApyDataGrid)
exports: []
#seuil (nombre de lignes retourné de la grille) de blocage des exports (par défaut = 0 (pas de limitation)) 
exports_limitation: 0
```

- Toutes ces options sont donc surchargeables au niveau des GridManagers, ou bien au niveau des grilles elle mêmes (GridBuilders).

Gestion de grilles par GridManager
------------------------

- Par exemple, dans un de vos controllers :

```
<?php

namespace Fia\RepositoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
...

/**
 * BusinessUnit controller.
 */
class BusinessUnitController extends CrudController
{
    /**
     * Index action.
     *
     * @Route("/businessunit", name="businessunit_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        return $this->redirect($this->generateUrl('businessunit_list'));
    }

    /**
     * Lists all BusinessUnit entities using fiducial_core service grid.
     *
     * @Route("/businessunit/list", name="businessunit_list")
     */
    public function listAction(Request $request)
    {
        //uses automated grid fiducialcore service
        $manager = $this
            ->get('fiducial_core.grid_manager')
            ->init(
                array(
                    'entity' => array(
                        array(
                            'id'     => 'businessunit',
                            'source' => 'FiaRepositoryBundle:BusinessUnit',
                            'read_only' => true
                        )
                    )
                )
            );
            
        //render
        return $manager->renderResponse('FiaRepositoryBundle:BusinessUnit:grid.html.twig');
    }
```

- La création de grille se fait suivant ces étapes:
    - récupération du service GridManager du FiducialCore via `$manager = $this->get('fiducial_core.grid_manager');`
    - paramétrage du grid manager via `$manager->init();`
    - rendu de la grille via `$manager->renderResponse();`
    
- L'interet de l'utilisation du GridManager est de pouvoir créer plusieurs grilles d'un seul coup, et de mutualiser/différencier leur configuration. Par exemple:

```
<?php
$manager = $this
    ->get('fiducial_core.grid_manager')
    ->init(
        array(
            //déclaration de 2 grilles de type entity
            'entity' => array(
                //grille pour l'entity FiaRepositoryBundle:BusinessUnit, d'id businessunit
                array(
                    'id'        => 'businessunit',
                    'source'    => 'FiaRepositoryBundle:BusinessUnit',
                    'read_only' => false,
                ),
                //grille pour l'entity FiaRepositoryBundle:Customer, d'id customer
                array(
                    'id'            => 'customer',
                    'source'        => 'FiaRepositoryBundle:Customer',
                    'show_filters'  => false
                )
            )
        ),
        //configuration commune aux grilles de ce manager
        array(
            'read_only'     => true,
            'show_filters'  => true
        )
    );
```

- On voit ici que deux grille de type *entity* (d'autres types: ODM, Vector sont à venir) sont déclarées (une pour les FiaRepositoryBundle:BusinessUnit, l'autre pour les FiaRepositoryBundle:Customer), ayant comme configuration commune **(2eme paramètre de la fonction init())**:
    - read_only = true
    - show_filters = true
- Au moment de définir ces grilles, on surcharges les valeurs de configuration comme suit: 
    - businessunit, on enlève la notion de read_only
    - customer, on désactive l'affichage des filtres
- Au final, les grilles auront comme configuration:
    - businessunit: read_only = false, show_filters = true (et toutes les configurations héritées du core non redéfinies)
    - customer: read_only = true, show_filters = false (et toutes les configurations héritées du core non redéfinies)
    
- **Modification de grilles d'un manager post déclaration**
    - Vous pourrez avoir besoin de modifier dynamiquement des grilles d'un manager post initialisation (pour y appliquer par example les méthodes du vendor), ou même d'en ajouter dynamiquement:
    - Le GridManager est fourni avec les fonctions publiques suivantes:
        - `getGridById($id)` : récupère une grille du manager par son id `$id`
        - `getAllGridIds()` : récupère la liste des ids des grilles déclarées dans un manager
        - `getAllGrids()` : récupère toutes les grilles d'un manager
        - `addGrid($builderType, $builderConfig)` : ajoute une grille de type `$builderType` avec la configuration `$builderConfig`
        - `addEntityGrid($builderConfig)` : ajoute une grille de type *entity* avec la configuration `$builderConfig`
    - Exemple : modification de la grille businessunit pour modifier ses filtres par défaut et modifier sa colonne d'actions
    
```
<?php
$manager = $this
    ->get('fiducial_core.grid_manager')
    ->init(
        array(
            'entity' => array(
                //grille pour l'entity FiaRepositoryBundle:BusinessUnit, d'id businessunit
                array(
                    'id'        => 'businessunit',
                    'source'    => 'FiaRepositoryBundle:BusinessUnit',
                    'read_only' => false,
                )
            )
        )
    );
    
//récupération de la grille businessunit depuis le manager
/** @var APY\DataGridBundle\Grid\Grid */
$businessUnitGrid = $manager->getGridById('businessunit');

//modification des filtres (fonction du vendor) de la grille businessunit
$businessUnitGrid->setDefaultFilters(
    array(
        'my_column' => array('operator' => 'eq', 'from' => 'my_default_value'),
    )
);

//modification de la colonne d'actions (fonction du vendor) de la grille businessunit
$businessUnitGrid->setActionsColumnSize(300);
$businessUnitGrid->setActionsColumnTitle('My New Title');
```

- **Gestion du rendu de la grille (via Twig)**
    - `renderResponse($view = null, $parameters = null, Response $response = null)`
        - `$view` : template twig pour rendu
        - `$parameters` : paramètres supplémentaires pour la vue twig
        - `$response` : object Response si necessaire
    - la particularité du manager est que la variable twig de la vue de rendu représentant chaque grille est composée comme suit : **grid_ID**
        - dans notre cas : `{{ grid_businessunit }}` (et `{{ grid_customer }}` pour la cas de nos grilles multiples )
    - exemple de rendu customisé via Twig :
    
```
<?php
//coté controlleur
return $manager->renderResponse(
    'FiaRepositoryBundle:BusinessUnit:grid.html.twig',
    array(
        'var1' => 'aaa',
        'user' => $this->get('security.token_storage')->getToken()->getUser()
    )
);
```

```
{# vue twig :  FiaRepositoryBundle:BusinessUnit:grid.html.twig #}
{% extends 'FiaRepositoryBundle::layout.html.twig' %}

{% block headline %}
    <span class="glyphicon glyphicon-home"></span>
    {{ '%element% list' |trans({'%element%': 'Business Units'|trans}) }}
{% endblock %}

{% block headline_actions %}
    {{ fiducialcore_generate_entity_actions() }}
{% endblock %}

{% block content %}
    <div class="row">
        {{ var1 }}
    <div>
    <div class="row">
        {{ 'Username'|trans }} : {{ user.username }}
    <div>
    {{ grid(grid_businessunit) }}
{% endblock %}
```

- La grille se rend par la fonction twig du vendor `{{ grid(grid_businessunit) }}`

Gestion de grille par GridBuilder
------------------------

- La mécanique de construction est très similaire au GridManager, mais ne permet que de créer une seule grille à la fois.
- Par exemple, dans un de vos controllers :

```
<?php

namespace Fia\RepositoryBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
...

/**
 * Customer controller.
 */
class CustomerController extends CrudController
{
    /**
     * Index action.
     *
     * @Route("/customer", name="customer_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        return $this->redirect($this->generateUrl('customer_list'));
    }

    /**
     * Lists all Customer entities using fiducial_core service grid.
     *
     * @Route("/customer/list", name="customer_list")
     */
    public function listAction(Request $request)
    {
        //uses automated grid fiducialcore service
        return $this->get('fiducial_core.grid_entity_builder')
                    ->init('FiaRepositoryBundle:Customer')
                    ->renderResponse('FiaRepositoryBundle:Customer:grid.html.twig');
    }
```

- La création de la grille se fait suivant ces étapes:
    - récupération du service GridBuilder du FiducialCore via `$manager = $this->get('fiducial_core.grid_entity_builder');`
    - paramétrage du grid builder via `init();`
    - rendu de la grille via `renderResponse();`
- Initialisation :
    - `init($source, $options = array())` :
        - `$source` : chaine de caratères, ou object APY\DataGridBundle\Grid\Source\Entity
        - `$options` : array de configurations de grilles (par défaut vide, donc héritée de la configuration FiducialCore)
        
- **Modification de la grille d'un builder post déclaration**
    - Vous pourrez avoir besoin de modifier dynamiquement la grille d'un builder post initialisation (pour y appliquer par example les méthodes du vendor):
    - Le Gridbuilder est fourni avec la fonction publique suivante:
        - `getGridBuilder()` : récupère l'object grille (APY\DataGridBundle\Grid\Grid)
    - Exemple : modification de la grille pour modifier ses filtres par défaut et modifier sa colonne d'actions
    
```
<?php
//uses automated grid fiducialcore service
$gridService = $this
    ->get('fiducial_core.grid_entity_builder')
    ->init('FiaRepositoryBundle:Customer');
    
//récupération de la grille depuis le builder
/** @var APY\DataGridBundle\Grid\Grid */
$customerGrid = $gridService->getGridBuilder();

//modification des filtres (fonction du vendor) de la grille
$customerGrid->setDefaultFilters(
    array(
        'my_column' => array('operator' => 'eq', 'from' => 'my_default_value'),
    )
);

//modification de la colonne d'actions (fonction du vendor) de la grille
$customerGrid->setActionsColumnSize(300);
$customerGrid->setActionsColumnTitle('My New Title');
```
        
- **Gestion du rendu de la grille (via Twig)**
    - `renderResponse($view = null, $parameters = null, Response $response = null)`
        - `$view` : template twig pour rendu
        - `$parameters` : paramètres supplémentaires pour la vue twig
        - `$response` : object Response si necessaire
    - la particularité du builder est que la variable twig de la vue de rendu représentant chaque grille **sera toujours `{{ grid }}`**
    - si vous voulez faire cohabiter des grilles crées par builder, passez sur l'utilisation du GridManager
    - Exemple de rendu customisé via Twig :
    
```
<?php
//coté controlleur
return $gridService->renderResponse(
    'FiaRepositoryBundle:Customer:grid.html.twig',
    array(
        'var1' => 'ccc'
    )
);
```

```
{# vue twig :  FiaRepositoryBundle:Customer:grid.html.twig #}
{% extends 'FiaRepositoryBundle::layout.html.twig' %}

{% block headline %}
    <span class="glyphicon glyphicon-customer"></span>
    {{ '%element% list' |trans({'%element%': 'Customers'|trans}) }}
{% endblock %}

{% block headline_actions %}
    {{ fiducialcore_generate_entity_actions() }}
{% endblock %}

{% block content %}
    <div class="row">
        {{ var1 }}
    <div>
    {{ grid(grid) }}
{% endblock %}
```
- La grille se rend par la fonction twig du vendor `{{ grid(grid) }}` .


Modification de la source de grille
------------------------

- Vous serez souvent confronté aux cas où vous devez customiser la source de grilles (préfiltrage, restrictions, etc.)
- les fonctions init() des GridManager et GridBuilder permettent la définition d'une source déja "travaillée" pour construire une grid.
- La classe de source pour les grilles de type *entity* est APY\DataGridBundle\Grid\Source\Entity
- Exemples:
    - Utiliser une source d'entité qui n'est pas sur le manager Doctrine configué par défaut (il faut utiliser la configuration manager_name pour automatiser les opérations Doctrine dans les Grids)
   
```
<?php
use APY\DataGridBundle\Grid\Source\Entity as GridEntity;
...

//customisation de source : l'entité TalentiaTalentiahrBundle:Collaborator est configurée sur le connecteur Doctrine "myCustomDoctrineManagerName"
$source = new GridEntity(
    'TalentiaTalentiahrBundle:Collaborator',
    'default',
    'myCustomDoctrineManagerName'
);

//avec GridManager
$manager = $this
    ->get('fiducial_core.grid_manager')
    ->init(
        array(
            'entity' => array(
                array(
                    'id'            => 'collaborator',
                    'source'        => $source,
                    'manager_name' => 'myCustomDoctrineManagerName'
                )
            )
        )
    );
    
//avec GridBuilder
$builder = $this
    ->get('fiducial_core.grid_entity_builder')
    ->init(
        $source,
        array(
            'manager_name' => 'myCustomDoctrineManagerName'
        )
    );
```

- Modifier le queryBuilder d'une source et l'utiliser dans un GridManager ou GridBuilder:
    
```
<?php
use APY\DataGridBundle\Grid\Source\Entity as GridEntity;
...

//customisation de source au niveau QueryBuilder pour n'avoir que les actifs
$source = new GridEntity('FiaRespositoryBundle:Customer');
$tableAlias = $source->getTableAlias();
$source->manipulateQuery(
    function ($query) use ($tableAlias)
    {
        $query->andWhere($tableAlias . '.active = 1');
    }
);


//avec GridManager
$manager = $this
    ->get('fiducial_core.grid_manager')
    ->init(
        array(
            'entity' => array(
                array(
                    'id'            => 'customer',
                    'source'        => $source
                )
            )
        )
    );
    
//avec GridBuilder
$builder = $this
    ->get('fiducial_core.grid_entity_builder')
    ->init($source);
```


Gestion des actions (row) automatisées
------------------------

- Les grilles permetent l'automatisation de créaction d'actions pour chaque ligne qu'elle contient (exemple : liens *edit*, *delete*, etc.)
- Ceci permet un couplage intuitif aux Cruds générés par FiducialCore, et **évite la redondance de code** pour naviguer dans ces Cruds
- Ces actions automatisées sont par défaut définies au niveau FiducialCore comme suit:
    - **edit** : lien vers l'edition d'une entité `{{ path('myentity_edit',{id: entity.id}) }}`
    - **delete** : lien vers la suppression d'une entité `{{ path('myentity_delete',{id: entity.id}) }}`
    - **see** : lien vers la visualisation d'une entité `{{ path('myentity_see,{id: entity.id}) }}`
- La grille gère seule si elle est en mode *readonly*:
    - si c'est le cas, elle ne propose pas *edit* et *delete*
    - si ce n'est pas le cas, elle propose toutes les actions présentées ci dessus
- Les actions (row) de base sont modifiables (à tous les niveaux de configuration des grilles) 
- Vous pouvez facilement ajouter / supprimer des actions dans la configuration des grilles, par example dans un GridBuilder:

```
<?php

//dans votre controller, on teste si l'user est ROLE_ADMIN pour pouvoir supprimer depuis la grille

use Fiducial\CoreBundle\Grid\Builders\AbstractBuilder;

/**
 * Workorder controller.
 */
class WorkorderController extends CrudController
{

    //...

    /**
     * Lists all Workorder entities using fiducial_core service grid.
     *
     * @Route("/workorder/list", name="workorder_list")
     */
    public function listAction(Request $request)
    {
        //automated routes
        $automatedRoutes = array(
            AbstractBuilder::FIDUCIALCORE_GRID_ROUTE_ACTION_SEE,
            AbstractBuilder::FIDUCIALCORE_GRID_ROUTE_ACTION_EDIT
        );
        //check if user is ROLE_ADMIN, if yes add delete action
        $canDelete = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN');
        if($canDelete){
            array_push(
                $automatedRoutes,
                AbstractBuilder::FIDUCIALCORE_GRID_ROUTE_ACTION_DELETE
            );
        }
        //uses automated grid fiducialcore service
        $gridService = $this
            ->get('fiducial_core.grid_entity_builder')
            ->init(
                'FiaWorkorderBundle:Workorder',
                array(
                    'automated_routes' => $automatedRoutes
                )
            );
            
        //...
        
    }
    
    //...
    
}
```

- Vous pouvez facilement ajouter des actions supplémentaires post initialisation de la grille

```
<?php

//dans votre controller, on ajoute une action supplémentaire de download au format PDF d'une entitté Workorder

use Fiducial\CoreBundle\Grid\Builders\AbstractBuilder;

/**
 * Workorder controller.
 */
class WorkorderController extends CrudController
{

    //...

    /**
     * Lists all Workorder entities using fiducial_core service grid.
     *
     * @Route("/workorder/list", name="workorder_list")
     */
    public function listAction(Request $request)
    {
        //uses automated grid fiducialcore service
        $gridService = $this
            ->get('fiducial_core.grid_entity_builder')
            ->init(
                'FiaWorkorderBundle:Workorder',
                array(
                    'automated_routes' => $automatedRoutes
                )
            );
            
        //gets grid builder
        $grid = $gridService->getGridBuilder();
        
        //add download as pdf column
        $pdfAction = new RowAction(
            $this->get('translator')->trans('Download'),
            'workorder_downloadAsPdf',
            false,
            '_self',
            array(
                'class' => 'btn btn-primary btn-xs',
                'icon' => 'download',
                'actions_style' => 'icon'
            )
        );
        $pdfAction->setRouteParameters(array('id'));
        //append pdf action
        $grid->addRowAction($pdfAction);
        
        //...
        
    }
    
    //...
    
}
```

Gestion des actions de masse automatisées
------------------------

- Les grilles permettent l'automatisation de création d'actions de masse (exemple : liens *massDelete*, etc.)
- Ceci permet un couplage intuitif aux Cruds générés par FiducialCore, et **évite la redondance de code** (les codes d'activation, archivage et suppression de masse sont génériques)
- Ces actions de masse automatisées sont par défaut définies au niveau FiducialCore comme suit:
    - **FiducialCoreBundle:Core:massActivate** : action de controlleur FiducialCoreBundle:Core massActivate()
    - **FiducialCoreBundle:Core:massArchive** : action de controlleur FiducialCoreBundle:Core massArchive()
    - **FiducialCoreBundle:Core:massDelete** : action de controlleur FiducialCoreBundle:Core massDelete()
- La grille detecte toute seule si elle affiche une source d'entité *SoftDeletable*:
    - si c'est le cas, propose les 3 actions vues ci dessus
    - si ce n'est pas le cas, ne propose que *massDelete*
- Les actions de masse de base sont modifiables (à tous les niveaux de configuration des grilles) 
- Vous pouvez facilement ajouter / supprimer des actions de masse dans la configuration des grilles, par example dans un GridBuilder:

```
<?php

//dans votre controller

/**
 * Workorder controller.
 */
class WorkorderController extends CrudController
{

    //...

    /**
     * Lists all Workorder entities using fiducial_core service grid.
     *
     * @Route("/workorder/list", name="workorder_list")
     */
    public function listAction(Request $request)
    {
        //automated routes
        $automatedMassActions = array(
            //define here some mass actions
        );
        //uses automated grid fiducialcore service
        $gridService = $this
            ->get('fiducial_core.grid_entity_builder')
            ->init(
                'FiaWorkorderBundle:Workorder',
                array(
                    'automated_mass_actions' => $automatedMassActions
                )
            );
            
        //...
        
    }
    
    //...
    
}
```

- Et comme pour les actions (row), vous pouvez ajouter post initialisation des actions de masse via `$gridService->getBridBuilder()->addMassAction()`
- Documentation sur l'ajout d'actions de masse du vendor [disponible ici](https://github.com/Abhoryo/APYDataGridBundle/blob/master/Resources/doc/grid_configuration/add_mass_action.md)

Manipulation des actions (row) déclarées
------------------------

- Vous rencontrerez certains cas ou vous aurez besoin de manipuler les row actions suivant le contenu de la ligne à laquelle elles se rattachent.
- Le moteur de grilles prévoit une méthode de récupération des row actions déclarées dans la grille (utile dans le cas des Crud ou les routes *see*, *edit* et *delete* sont pré-déclarées)
    - **getRowActionByRoute($route)** : récupère l'action dont la route matche le paramètre $route (exemple, si $route='see', l'action 'entity_see' sera retournée)
    
- **Exemple d'utilisation** : on veut masquer l'action d'edition dans une grille d'entité ou la valeur du champ *ttcAmount* de l'entité est *différent de 1000*

```
<?php

use Fiducial\CoreBundle\Grid\Builders\AbstractBuilder;

class OdController extends CrudRESTController
{

    //...

    /**
     * @inheritdoc
     */
    protected function customizeGrid(Request $request, Grid $grid)
    {
        //manipulate edit row action
        $actionEdit = $grid->getRowActionByRoute(AbstractBuilder::FIDUCIALCORE_GRID_ROUTE_ACTION_EDIT);
        if(!is_null($actionEdit)){
            $actionEdit->manipulateRender(
                function ($action, $row)
                {
                    if ($row->getField('ttcAmount') != 1000) {
                        return null;
                    }else{
                        return $action;
                    }
                }
            );
        }
    }
    
    //...
    
}
```

- Toute autre manipulation sur les row actions y est possible (notamment celles décrites dans la [documentation technique du vendor](https://github.com/Abhoryo/APYDataGridBundle/tree/master/Resources/doc))