Contexte utilisateur : CAS (Central Authentication Service)
======

Configuration CAS (Central Authentication Service)
-----------------

- Prérequis :

Pour réaliser l'authentification avec le CAS, il faut rajouter le bundle [besimple SSO](https://github.com/BeSimple/BeSimpleSsoAuthBundle/blob/master/Resources/doc/index.md) :

Ce bundle sert de passerelle entre le service d'authentification de Symfony2 et l'authentification via le CAS.

Il suffit de le rajouter dans son composer.json :

```
    "require": {
        "php": ">=5.3.3",
        "symfony/symfony": "2.7.*",
        ...
        "besimple/sso-auth-bundle": "dev-master"
    },
```

et de relancer un composer install. Une fois téléchargé, il faut déclarer é Symfony dans AppKernel votre nouveau bundle :

```
new BeSimple\SsoAuthBundle\BeSimpleSsoAuthBundle(),
```

- Config.yml

Dans le fichier config.yml de votre application, rajouter la configuration de be simple :

```
# be_simple for CAS auth    
be_simple_sso_auth:
    fiducas:
        protocol:
            id: cas
            version: 2
        server:
            id: cas
            login_url: https://lxlyodev30.fiducial.dom/CASServer/login
            logout_url: https://lxlyodev30.fiducial.dom/CASServer/logout
            validation_url: https://lxlyodev30.fiducial.dom/CASServer/serviceValidate
```

(Les URL indiquent le serveur sur lequel se trouve le CAS)

Il faut maintenant spécifier é fos_user votre propre user et le faire hériter de CASUserModel é la place de celui par défaut du FiducialCore :


```
fos_user:
    ...
    user_class: monBundle\MainBundle\Entity\Profile
    ...
```

L'entity Profile doit étendre CASUSerModel. Rajouter les annotations suivantes et l'extend.


```
use Fiducial\UserBundle\Model\CAS\CASUserModel;
use Doctrine\ORM\Mapping as ORM;
use Fiducial\CoreBundle\Annotations\Entity as FCEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="monBunble_cas_profile")
 * @ORM\Entity(repositoryClass="monBundle\MainBundle\Entity\ProfileRepository")
 */
class Profile extends CASUserModel
{

}
```

*Note:*   Dans monBunble\MainBundle\Entity\ si l'entité Profile n'existe pas, il faut la créer (sf fiducialcore:generate:entity puis sf fiducialcore:generate:crud).

- fiducial_core.yml

On prévient le core d'utiliser notre entité é la place de celle par défaut 

```
    #Fiducial user settings
    fiducial_user:
        cas_sso_auth:
            entity: 'monBundleMainBundle:Profile'
```

- Security.yml

Dans ce fichier de configuration, on ajoute le nouveau provider : 

```
    providers:
        ...
        fiducialcore_cas_user_provider:
            id: fiducial_user.cas_user_provider
```

et on configure le firewall comme il faut :

```
    firewalls:
        main:
            pattern: ^/
            trusted_sso:
                manager: fiducas
                login_action: false
                logout_action: false
                create_users: true
                created_users_roles: [ROLE_USER, ROLE_ADMIN]
                login_path: /login
                check_path: /login_check
                default_target_path: /
                always_use_default_target_path: true
            provider: fiducialcore_cas_user_provider
            logout:
                path: /logout
                target: /
```                

Vider le cache de symfony et le cache de votre navigateur (cookies de session)