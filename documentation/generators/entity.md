Générateur : Entity
===================

Sommaire:
-------------

- [Présentation](#toc_2)
- [Créer le code de l'entité en tant que Timestampable](#toc_3)
- [Créer le code de l'entité en tant que SoftDeletable](#toc_4)
- [Créer le code de l'entité en tant que Searchable](#toc_5)


Présentation:
--------------------------------------------------------------------

- Disponible via la commande:

    ```
    $ php app/console fiducialcore:generate:entity
    ```
    
    
Il étend la mécanique du générateur **doctrine:generate:entity**, mais offrant quelques fonctionnalités supplémentaires.

Il est pour l'instant recommandé d'utiliser la configuration par **annotations**, car les fonctionnalités supplémentaires en sont dépendantes.

Lors de la création de votre entité, il vous sera demandé si vous voulez:
    - préprarer l'entité en tant que **TimeStampable**
    - préprarer l'entité en tant que **SoftDeletable**
    - préprarer l'entité en tant que **Searchable**

Créer le code de l'entité en tant que Timestampable:
--------------------------------------------------------------------

- permet l'enregistrement automatique des dates de création et modification d'un objet décrit par cette entité (par évenements doctrine)
- ajoute automatiquement deux propriétés à l'entité : **$createdAt** et **$updatedAt** (contenues dans le trait PHP)
- ajoute automatiquement l'annotation **@ORM\HasLifecycleCallbacks** de Doctrine
- ajoute automatiquement l'utilisation du trait PHP **Fiducial\CoreBundle\Entity\Traits\TimestampableTrait**

Example : extrait de code généré

```
<?php

namespace My\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fiducial\CoreBundle\Entity\Traits\TimestampableTrait;
use Fiducial\CoreBundle\Entity\Interfaces\CoreEntityInterface;

/**
* Entity.
*
* @ORM\Table()
* @ORM\Entity(repositoryClass="My\MainBundle\Entity\EntityRepository")
* @ORM\HasLifecycleCallbacks
*/
class Entity implements CoreEntityInterface
{

  /**
   * handle timestamp markers eligibility
   * @see TimestampableTrait
   */
  use TimestampableTrait;
  
  //...
  
  /**
   * @return string
   */
  public static function getEntityName()
  {
      return 'entity';
  }

  /**
   * @return string
   */
  public static function getEntityIconClass()
  {
      return 'cog';
  }

  /**
   * @return string
   */
  public static function getEntityRoutePrefix()
  {
      return 'entity';
  }

  //...
  
}
```

Aperçu du contenu des actions automatisées du trait:

```
<?php

namespace Fiducial\CoreBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
* Injectable trait for doctrine entities to handle generic timetamps
* Class TimestampableTrait.
*/
trait TimestampableTrait
{

  ...
    
  /**
   * @ORM\PrePersist()
   */
  public function handleCreatedAt()
  {
      if (is_null($this->getCreatedAt())) {
        $this->setCreatedAt(new \DateTime('now'));
      }
  }
    
  /**
   * @ORM\PreUpdate()
   */
  public function handleUpdatedAt()
  {
      $this->setUpdatedAt(new \DateTime('now'));
  }
  
  ...
}
```
  
Créer le code de l'entité en tant que SoftDeletable:
--------------------------------------------------------------------

- permet la suppression logique (désactivation) d'un objet décrit par cette entité (par évenements doctrine)
- ajoute automatiquement deux propriétés à l'entité : **$archived** et **$archivedAt** (contenues dans le trait PHP)
- ajoute automatiquement l'annotation **@ORM\HasLifecycleCallbacks** de Doctrine
- ajoute automatiquement l'utilisation du trait PHP **Fiducial\CoreBundle\Entity\Traits\SoftDeletableTrait**

Example de code généré:

```
<?php

namespace My\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fiducial\CoreBundle\Entity\Traits\SoftDeletableTrait;
use Fiducial\CoreBundle\Entity\Interfaces\CoreEntityInterface;

/**
* Entity.
*
* @ORM\Table()
* @ORM\Entity(repositoryClass="My\MainBundle\Entity\EntityRepository")
* @ORM\HasLifecycleCallbacks
*/
class Entity implements CoreEntityInterface
{

  /**
   * handle soft deletion eligibility
   * @see SoftDeletableTrait
   */
  use SoftDeletableTrait;

  //...
  
  /**
   * @return string
   */
  public static function getEntityName()
  {
      return 'entity';
  }

  /**
   * @return string
   */
  public static function getEntityIconClass()
  {
      return 'cog';
  }

  /**
   * @return string
   */
  public static function getEntityRoutePrefix()
  {
      return 'entity';
  }

  //...

}
```

Aperçu du contenu des actions automatisées du trait:

```
<?php

namespace Fiducial\CoreBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;

/**
* Injectable trait for doctrine entities to handle soft deletion
* Class SoftDeletableTrait.
*/
trait SoftDeletableTrait
{

  ...

  /**
   * Handle archive toggling.
   *
   * @ORM\PrePersist()
   * @ORM\PreUpdate()
   */
  public function handleArchive()
  {
      if ($this->getArchived()) {
          $this->setArchivedAt(new \DateTime('now'));
      } else {
          $this->setArchivedAt(null);
      }
  }

  ...
}
```

Il ne vous reste plus qu'a faire un **doctrine:schema:update --force --dump-sql** pour que doctrine crée automatiquement ces champs supplémentaires sur la table cible de votre entité.

Créer le code de l'entité en tant que Searchable:
--------------------------------------------------------------------

- permet l'accessibilité au queryBuilders du moteur de recherche FiducialCore à cette entité
- ajoute automatiquement l'annotation **@CoreEntity\Searchable(fields={"*})**, indiquant que l'entité est recherchable sur tous ses champs qui ne sont pas des mappings vers d'autres entités
- ajoute automatiquement l'utilisation du pool d'anotations type Entity de FiducialCore **Fiducial\CoreBundle\Annotations\Entity as CoreEntity**
- ajoute automatiquement l'utilisation de l'interface PHP **Fiducial\CoreBundle\Entity\Interfaces\ListableEntityInterface** (pour pouvoir être rendue dans la liste de résulats du moteur de recherche)

Example de code généré:

```
<?php

namespace My\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fiducial\CoreBundle\Entity\Traits\SoftDeletableTrait;
use Fiducial\CoreBundle\Entity\Interfaces\CoreEntityInterface;

/**
* Entity.
*
* @ORM\Table()
* @ORM\Entity(repositoryClass="My\MainBundle\Entity\EntityRepository")
* @ORM\HasLifecycleCallbacks
* @CoreEntity\Searchable(fields={"*"})
*/
class Entity implements ListableEntityInterface
{

  /**
   * handle soft deletion eligibility
   * @see SoftDeletableTrait
   */
  use SoftDeletableTrait;

  //...
  
  /**
   * @return string
   */
  public static function getEntityName()
  {
      return 'entity';
  }

  /**
   * @return string
   */
  public static function getEntityIconClass()
  {
      return 'cog';
  }

  /**
   * @return string
   */
  public static function getEntityRoutePrefix()
  {
      return 'entity';
  }

  /**
   * @return string
   */
  public function renderAsListableEntity()
  {
      return '<strong>'.$this->getLabel().'</strong>';
  }

  /**
   * @return array
   */
  public function getListableEntityActions()
  {
     return array(
          'see' => array(
              'route' => $this->getEntityRoutePrefix().'_see',
              'params' => array('id' => $this->getId()),
              'color' => 'info',
              'icon' => 'search',
          ),
      );
  }

//...

}
```
