Générateur : Bundle
===================

Utilisation du générateur de Bundles du Fiducial Core:
--------------------------------------------------------------------

- Disponible via la commande:

    ```
    $ php app/console fiducialcore:generate:bundle
    ```
    
Coming soon.