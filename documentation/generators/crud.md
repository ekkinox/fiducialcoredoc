Générateur : CRUD
===================

Sommaire:
-------------

- [Présentation](#toc_2)
- [Compatibilité web services REST](#toc_3)
- [Customisation du controller](#toc_4)
    - [Customiser action "see" (visualisation entité)](#toc_5)
    - [Customiser action "list" (liste grille entités)](#toc_6)
    - [Customiser actions "add" et "edit" (formulaires de création et modification entité)](#toc_7)
    - [Customiser action "delete" (suppression entité)](#toc_8)

Présentation
--------------------------------------------------------------------

- Disponible via la commande:

```
$ php app/console fiducialcore:generate:crud
```
     
- Il étend la mécanique du générateur **doctrine:generate:crud**, mais offrant quelques fonctionnalités supplémentaires.
- Il est pour l'instant recommandé d'utiliser la configuration par **annotations**, car les fonctionnalités supplémentaires en sont dépendantes.
- Exemple de code *controller* généré:

```
<?php

namespace My\MainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Fiducial\CoreBundle\Entity\Controllers\CrudController;

use My\MainBundle\Entity\Entity;
use My\MainBundle\Form\EntityType;

/**
 * Entity controller.
 * Generated with FiducialCore Crud Generator
 */
class EntityController extends CrudController
{

    /**
     * @inheritdoc
     */
    public function getEntityName()
    {
        return 'entity';
    }

    /**
     * @inheritdoc
     */
    public function getEntityRepositoryName()
    {
        return 'MyMainBundle:Entity';
    }

    /**
     * @Route("/entity", name="entity_index")
     * @Method("GET")
     *
     * Index action
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction(Request $request)
    {
        return $this->processRedirectToGrid();
    }

    /**
     * @Route("/entity/list", name="entity_list")
     *
     * Lists all Entity entities using Grid
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        return $this->processGrid($request);
    }

    /**
     * @Route("/entity/see/{id}", name="entity_see")
     * @Method("GET")
     *
     * Finds and displays a Entity entity.
     * @param Request $request
     * @param Entity $entity
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function seeAction(Request $request, Entity $entity)
    {
        return $this->processView($request, $entity);
    }

    /**
     * @Route("/entity/add", name="entity_add")
     *
     * Displays a form to add a new Entity entity.
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction(Request $request)
    {
        return $this->processForm(
            $request,
            new EntityType(),
            new Entity()
        );
    }

    /**
     * @Route("/entity/edit/{id}", name="entity_edit")
     *
     * Displays a form to edit an existing Entity entity.
     * @param Request $request
     * @param Entity $entity
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, Entity $entity)
    {
        return $this->processForm(
            $request,
            new EntityType(),
            $entity,
            self::ACTION_EDIT
        );
    }

    /**
     * @Route("/entity/delete/{id}", name="entity_delete")
     *
     * Deletes a Entity entity.
     * @param Request $request
     * @param Entity $entity
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Entity $entity)
    {
        return $this->processDelete($request, $entity);
    }

}
```
  
- Liste exhaustive des actions (méthodes type Routes) générées:
    - **addAction()** : affiche le FormType en mode création
    - **deleteAction()** : méthode pour la suppression depuis le CRUD
    - **editAction()** : affiche le FormType en mode édition
    - **indexAction()** : redirige automatiquement sur listAction()
    - **listAction()** : affiche la DataGrid FiducialCore correspondant aux configurations du core et à celle du CRUD (readonly, annotations entité, etc.)
    - **seeAction()** : affiche le détail d'une entité
  
- Liste exhausitve des fichiers générés dans le bundle de l'entité (ex : pour entité Entity):
    - fichier **EntityController.php** dans repertoire Controller
    - fichier **EntityType.php** dans repertoire Form (si non existant)
    - vues du CRUD dans repertoire **Resources/views/Entity** (see.html.twig, add.html.twig, etc.)
    - controller EntityControllerTest.php de test dans **Tests/Controller**
  
- **Note**: le CRUD généré prends en compte si le CRUD doit être **readonly** (non support des routes *add*, *edit* et *delete*)

- Pour la partie listing, le composant **Grid de FiducialCore** est utilisé, répondant à la route *_list* (ou bien *_index* qui redirige par défaut sur *_list*)


Compatibilité web services REST
--------------------------------------------------------------------

- Lors de la création de votre CRUD sur une entité, il vous sera demandé si vous voulez générer **l'exposition REST du CRUD**:
    - n'étend plus **Fiducial\CoreBundle\Entity\Controllers\CrudController** mais  **Fiducial\CoreBundle\Entity\Controllers\CrudRESTController**, ce qui permet:
        - l'exposition via le protocole de webservices REST de l'entitié (actions *get* et *cget*)
        - l'adaptation automatique du fichier **Resources\config\rest\routing.yml** du bundle pour déclarer les routes REST
- Le code généré est donc sensiblement identique:

```
<?php

namespace My\MainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Fiducial\CoreBundle\Entity\Controllers\CrudRESTController;

use My\MainBundle\Entity\Entity;
use My\MainBundle\Form\EntityType;

/**
 * Entity controller.
 * Generated with FiducialCore Crud Generator
 */
class EntityController extends CrudRESTController
{
    //...
}
```

Customisation du controller
--------------------------------------------------------------------

- **Rappel** : le générateur de CRUD vous offre un controlleur certes prêt à l'emploi, mais qui à pour vocation d'etre modifié par vos soins pour répondre à vos besoins applicatifs.
- Avant de commencer cette section, gardez bien à l'esprit que le code généré fait appel à des fonctions d'automatisation de process du Crud (traductions, messages retours, propulsions de grilles, etc.)
    - vous pouvez tout à fait completement remplacer si besoin est le code généré par vos propres méthodes, la base utilisée étant complétement du Symfony2 natif.
- La couche PHP objet utilisée prévoit l'implémentation de la surcharge de fonctions du trait **CrudControllerTrait** (Fiducial\CoreBundle\Entity\Controllers\Traits\CrudControllerTrait.php)
    - N'hesitez pas lors de vos developpements à lire cette source pour bien comprendre son fonctionnement
    
### Customiser action "see" (visualisation entité)

- Le code controller généré pour cette action est le suivant (exemple):

```
<?php

/**
 * @Route("/article/see/{id}", name="article_see")
 * @Method("GET")
 *
 * Finds and displays a Article entity.
 * @param Request $request
 * @param Article $entity
 * @return \Symfony\Component\HttpFoundation\Response
 */
public function seeAction(Request $request, Article $entity)
{
    return $this->processView($request, $entity);
}
```

- La méthode processView() se présente ainsi dans le trait **CrudControllerTrait**:

```
<?php
/**
 * Auto process ({entityName}_see) route
 * @param Request $request
 * @param $entity : Doctrine entity
 * @return mixed
 */
protected function processView(Request $request, $entity)
{
    //init render params
    $renderParams = array(
        'entity' => $entity,
        'entity_label' => $this->getEntityLabel(),
        'entity_plural_label' => $this->getEntityPluralLabel(),
        'entity_icon' => $this->getEntityIconClass()
    );
    //customize render params
    $renderParams = $this->customizeProcessViewRenderParams($request, $renderParams);
    //render
    return $this->render(
        $this->getCrudTemplate(CrudControllerInterface::ACTION_SEE),
        $renderParams
    );
}
```

- On observe l'existence de la fonction **customizeProcessViewRenderParams($request, $renderParams)**
    - $request : object Request Symfony2
    - $renderParams : tableau de paramètres prérempli à enrichir/redéfinir si besoin
- Par défaut, elle ne fait que fowarder les paramètres à injecter a la template de visualisation de l'entité
- Vous pouvez surcharger cette methode dans votre controlleur pour modifier les parametres injectés à la template de visualisation (il faut retourner un merge avec les paramètres récupérés)
- Exemple : ajout d'une variable {{ articleLabel }} à destination de la template
    
    ```
    <?php
    
    /**
     * @Route("/article/see/{id}", name="article_see")
     * @Method("GET")
     *
     * Finds and displays a Article entity.
     * @param Request $request
     * @param Article $entity
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function seeAction(Request $request, Article $entity)
    {
        return $this->processView($request, $entity);
    }
    
    /**
     * @inheritdoc
     */
    protected function customizeProcessViewRenderParams(Request $request, $renderParams)
    {
        $entity = $renderParams['entity'];
        return array_merge(
            $renderParams,
            array(
                'articleLabel' => $entity->getLabel();
            )
        );
    }
    ```
    
### Customiser action "list" (liste grille entités)

- Le code controller généré pour cette action est le suivant (exemple):

```
<?php

/**
 * @Route("/article/list", name="article_list")
 *
 * Lists all Article entities using Grid
 * @param Request $request
 * @return \Symfony\Component\HttpFoundation\Response
 */
public function listAction(Request $request)
{
    return $this->processGrid($request);
}
```

- La méthode processGrid() fait notamment appel à deux methodes de customisation dans le trait **CrudControllerTrait**
    - **customizeGridSource(Request $request, GridEntity $source)** : pour customiser la source de la grille
        - $request : object Request Symfony2
        - $source : object GridEntity (source de grilles utilisée par le générateur de grilles)
    - **customizeGrid(Request $request, Grid $grid)** : pour customiser la grille en elle même
        - $request : object Request Symfony2
        - $grid : grid builder FiducialCore
    - **customizeProcessGridRenderParams(Request $request, $renderParams)** : pour customiser les paramètres à injecter à la template de la grille
        - $request : object Request Symfony2
        - $renderParams : tableau de paramètres prérempli à enrichir/redéfinir si besoin
    
- Exemples:
    - **customisation de la source de la grille** : on manipule la source de la grille avant de la retourner
    
    ```
    <?php
    
    /**
     * @Route("/article/list", name="article_list")
     *
     * Lists all Article entities using Grid
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        return $this->processGrid($request);
    }
    
    /**
     * @inheritdoc
     */
    public function customizeGridSource(Request $request, GridEntity $source)
    {
        $authChecker = $this->get('security.authorization_checker');
        $hasAllAccess = $authChecker->isGranted('ROLE_ADMIN') || $authChecker->isGranted('ROLE_HEAD');
        if (!$hasAllAccess) {
            //current user name
            $userName = $this->getUser()->getUsername();
            //init query
            $qryCollaborator = $this
                ->getEntityManager()
                ->getRepository($source->getEntityName())
                ->createDefaultQueryBuilder($source, $userName);
            //affect to source
            $source->initQueryBuilder($qryCollaborator);
        }
        //return customized source
        return $source;
    }
    ```
    
    - **customisation de la grille en elle même** : on redéfinit l'ordre des colonnes
    
    ```
    <?php
    
    /**
     * @Route("/article/list", name="article_list")
     *
     * Lists all Article entities using Grid
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        return $this->processGrid($request);
    }
    
    /**
     * @inheritdoc
     */
    protected function customizeGrid(Request $request, Grid $grid)
    {
        //change column order
        $grid->setColumnsOrder(array('id','status'));
        //return self
        return $this;
    }
    ```
    
    - **customisation des paramètres à injecter à la template de la grille** : on ajoute par exemple des infos utilisateur ( *{{ userName }}* )
    
    ```
    <?php
    
    /**
     * @Route("/article/list", name="article_list")
     *
     * Lists all Article entities using Grid
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request)
    {
        return $this->processGrid($request);
    }
    
    /**
     * @inheritdoc
     */
    protected function customizeProcessGridRenderParams(Request $request, $renderParams)
    {
        //get user name
        $user = $this->getUser();
        return array_merge(
            $renderParams,
            array(
                'userName' => $user->getUsername();
            )
        );

    }
    ```
    
### Customiser actions "add" et "edit" (formulaires de création et modification entité)

- Le code controller généré pour cette action est le suivant (exemple):

```
<?php
/**
 * @Route("/article/add", name="article_add")
 *
 * Displays a form to add a new Article entity.
 * @param Request $request
 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
 */
public function addAction(Request $request)
{
    return $this->processForm(
        $request,
        new ArticleType(),
        new Article()
    );
}

/**
 * @Route("/article/edit/{id}", name="article_edit")
 *
 * Displays a form to edit an existing Article entity.
 * @param Request $request
 * @param Article $entity
 * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
 */
public function editAction(Request $request, Article $entity)
{
    return $this->processForm(
        $request,
        new ArticleType(),
        $entity,
        self::ACTION_EDIT
    );
}
```

- Dans ces deux actions, la methode **processForm()** est utilisée. Elle fait appel en interne à:
    - **customizeProcessFromProcessing(Request $request, $entityManager, $entity, $action)** : pour customiser les intéractions lors du traitement du formulaire valide et soumis **juste avant de persister l'entité du Crud**
        - $request : object Request Symfony2
        - $entityManager : manager d'entités doctrine préparé pour manipulations de l'entité
        - $entity : entité du Crud sur laquelle on agit
        - $action : mode d'edition suivant la provenance de l'appel (CrudControllerInterface::ACTION_ADD ou CrudControllerInterface::ACTION_EDIT)
    - **customizeProcessFromRenderParams(Request $request, $renderParams, $action)** : pour customiser les paramètre à injecter à la template de rendu du formulaire (modes add et edit)
        - $request : object Request Symfony2
        - $renderParams : tableau de paramètres prérempli à enrichir/redéfinir si besoin
        - $action : mode d'edition suivant la provenance de l'appel (*self::ACTION_ADD* ou *self::ACTION_EDIT*)
    
- Exemples:
    - **customisation du traitement du formulaire** : on modifie l'entité avant de la persister sur son label
    
    ```
    <?php
    /**
     * @Route("/article/add", name="article_add")
     *
     * Displays a form to add a new Article entity.
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction(Request $request)
    {
        return $this->processForm(
            $request,
            new ArticleType(),
            new Article()
        );
    }
    
    /**
     * @Route("/article/edit/{id}", name="article_edit")
     *
     * Displays a form to edit an existing Article entity.
     * @param Request $request
     * @param Article $entity
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, Article $entity)
    {
        return $this->processForm(
            $request,
            new ArticleType(),
            $entity,
            self::ACTION_EDIT
        );
    }
    
    /**
     * @inheritdoc
     */
    protected function customizeProcessFromProcessing(Request $request, $entityManager, $entity, $action)
    {
        //calulates label
        $label = $action == self::ACTION_EDIT ? 'article.edited' : 'article.added';
        //change label
        $entity->setLabel(
            $entity->getLabel() . $this->get('translator')->trans($label)
        );
        //foward entity for later persist
        return $entity;
    }
    ```
    
    - **customisation des paramètres à injecter à la template du fomulaire** : on ajoute par exemple une variable {{ myVar }} dépendante de l'action en cours (add ou edit)
    
    ```
    <?php
    /**
     * @Route("/article/add", name="article_add")
     *
     * Displays a form to add a new Article entity.
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addAction(Request $request)
    {
        return $this->processForm(
            $request,
            new ArticleType(),
            new Article()
        );
    }
    
    /**
     * @Route("/article/edit/{id}", name="article_edit")
     *
     * Displays a form to edit an existing Article entity.
     * @param Request $request
     * @param Article $entity
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, Article $entity)
    {
        return $this->processForm(
            $request,
            new ArticleType(),
            $entity,
            self::ACTION_EDIT
        );
    }
    
    /**
     * @inheritdoc
     */
    protected function customizeProcessFromRenderParams(Request $request, $renderParams, $action)
    {   
        return array_merge(
            $renderParams,
            array(
                'myVar' => $action == self::ACTION_EDIT ? true : false;
            )
        );
    }
    ```
    
    
 ###  Customiser action "delete" (suppression entité)


- Le code controller généré pour cette action est le suivant (exemple):

```
<?php
/**
 * @Route("/article/delete/{id}", name="article_delete")
 *
 * Deletes a Article entity.
 * @param Request $request
 * @param Article $entity
 * @return \Symfony\Component\HttpFoundation\RedirectResponse
 */
public function deleteAction(Request $request, Article $entity)
{
    return $this->processDelete($request, $entity);
}
```

- La methode **processDelete()** est utilisée. Elle fait appel en interne à:
    - **customizeProcessDelete(Request $request, $entityManager, $entity)** : pour customiser le process de suppression juste avant que l'entité ne soit supprimée
        - $request : object Request Symfony2
        - $entityManager : manager d'entités doctrine préparé pour manipulations de l'entité
        - $entity : entité du Crud sur laquelle on agit
        
- Exemple:
    - **customisation du process de suppression** : on décrémente un stock pour un article supprimé
    
    ```
    <?php
    /**
     * @Route("/article/delete/{id}", name="article_delete")
     *
     * Deletes a Article entity.
     * @param Request $request
     * @param Article $entity
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Article $entity)
    {
        return $this->processDelete($request, $entity);
    }
    
    /**
     * @inheritdoc
     */
    protected function customizeProcessDelete(Request $request, $entityManager, $entity)
    {
        //fetch related stock
        $stock = $entityManager
            ->getRepository('MyMainBundle:Stock')
            ->findByType($entity->getType());
        //decrement & save
        $stock->setQty($stock->getQty() - 1);
        $entityManager->persist($stock);
        $entityManager->flush();
        //foward entity
        return $entity;
    }
    ```