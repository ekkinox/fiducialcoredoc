Système de notifications FiducialCore
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [Configuration](#toc_3)
- [Zone de lecture des notifications](#toc_4)
- [Création notification par CRUD](#toc_5)
- [Notifications manager - service FiducialCore](#toc_6)

Présentation:
-------------

- Le FiducialCore est livré avec un système de notifications souple qui peut etre utilisé pour des messages propres à vos apllications tout comme un point de communications admins vers utilisateurs.
- FiducialCore propose une extension twig pour pouvoir très simplement fournir une zone de lecture de notifications pour vos utilisateurs.
- FiducialCore propose aussi un CRUD automatisé de gestion de ces notifications, accèssible dans le menu depuis:

```
#app/config/fiducial_core.yml
fiducial_core:
    menu:
        admin_menu_notification_management: true
```
    
Configuration:
----------------------------------------------------

- Les options de configuration disponibles sont les suivantes (avec leur valeurs par défaut):

```
#app/config/fiducial_core.yml
fiducial_core:
    notification:
        #active système des notifications
        enable_notification:  true
        #hauteur maximum de la card dashboard des notifications (en pixel, si = 0, pas de limites)
        board_max_height: 350
        #template de la card dashboard pour visualiser les notifications
        board_template: 'FiducialCoreBundle:Notification:Board/index.html.twig'
        #template du contenu ajax de la card dashboard pour visualiser les notifications
        list_template: 'FiducialCoreBundle:Notification:Board/notifications.html.twig'

```

Zone de lecture des notifications :
----------------------------------------------------

- Vous pouvez très simplement fournir à vos utilisateurs une zone de lecture des notifications
    - ce via l'appel d'un extension Twig du FiducialCore dans vos templates:
    `{{ fiducialcore_generate_notification_board() }}`
    - vous pouvez faire cohabiter plusieurs instances de cette extension dans la même page, leurs DOM étant indépendants niveau ids HTML (pas de collisions ajax)


Création notification par CRUD :
----------------------------------------------------

- Si vous avez activé le dans le menu admin (`fiducial_core.menu.admin_menu_notification_management`), vous pouvez via le menu accèder à un CRUD de gestion des notifications.
- Via ce CRUD, vous pouvez créer, editer, supprimer des ntofications
- Les entités représentant les notifications sont **SoftDeletable**, donc supprimer logiquement une notification aura pour résultat qu'elle ne sera plus disponible dans les zones de lectures.
- Lors de la création, vous pouvez indiquer:
    - **un titre**
    - **un message** (avec éditeur wysiwyg) : si un message est saisi, la zone de lecture pour cette notification proposera d'afficher ce message dans une fenêtre modale (avec son rendu HTML)
    - **une sévérité** (niveau de la notification), parmi:
        - *defaut* : notification par défaut (constante Fiducial\CoreBundle\Entity\Notification::SEVERITY_DEFAULT)
        - *information* : notification d'information (constante Fiducial\CoreBundle\Entity\Notification::SEVERITY_INFO)
        - *succès* : notification de succès (constante Fiducial\CoreBundle\Entity\Notification::SEVERITY_SUCCESS)
        - *avertissement* : notification type avertissement (constante Fiducial\CoreBundle\Entity\Notification::SEVERITY_WARNING) 
        - *erreur* : notification type erreur (constante Fiducial\CoreBundle\Entity\Notification::SEVERITY_DANGER)
- Les différents types d'affectations sont:
    - **Tout le monde** : pour tous les utilisateurs de l'application (constante Fiducial\CoreBundle\Entity\Notification::TARGET_ALL)
    - **Utilisateurs** : pour certains utilisateurs ciblés (constante Fiducial\CoreBundle\Entity\Notification::TARGET_USERS)
    - **Groupes** : pour l'ensemble des utilisateurs appartenant aux groupes séléctionnés (constante Fiducial\CoreBundle\Entity\Notification::TARGET_GROUPS)
    
    
Notifications manager - service FiducialCore :
----------------------------------------------------

- FiducialCore livre un service Symfony2 permettant d'intéragir avec le système de notifications: **fiducial_core.notification_manager** (notifications manager)
- Il permet aux developpeurs:
    - **la création de nouvelles notifications**
        - Fonction du manager : *createNotification*
    
        ```
        <?php
        //Fiducial\CoreBundle\Service\Notification\Manager.php
        /**
         * Creates a new notification and its associations
         * @param $title : notification title
         * @param $message : notification message
         * @param int $targetType : type of target for notification associations
         * @param string $severity : notification severity
         * @param $targets : targets
         * @param $tag : notification tag
         * @return Notification
         */
        public function createNotification(
            $title,
            $message = null,
            $targetType = Notification::TARGET_ALL,
            $severity = Notification::SEVERITY_DEFAULT,
            $targets = array(),
            $tag = null
        ){
            //...
        }
        ```
        
        - exemples:
        
        
        ```
        <?php
        use Fiducial\CoreBundle\Entity\Notification;
        
        //création d'une notification type erreur pour tous les utilisateurs depuis un controlleur
        $notification = $this
            ->get('fiducial_core.notification_manager')
            ->createNotification(
                'Mon titre erreur',
                'Mon message erreur',
                Notification::TARGET_ALL,
                Notification::SEVERITY_DANGER
               
            );
            
        //création d'une notification type succès, sans message,  pour l'utilisateur courant depuis un controlleur
        $user = $this->getUser();
        $notification = $this
            ->get('fiducial_core.notification_manager')
            ->createNotification(
                'Mon titre ok',
                null,
                Notification::TARGET_USERS,
                Notification::SEVERITY_SUCCESS,
                $user
            );
            
        //création d'une notification type averstissement pour les groupes ROLE_ADMIN et ROLE_MANAGER et avec un tag "My_TAG", depuis un controlleur
        $notification = $this
            ->get('fiducial_core.notification_manager')
            ->createNotification(
                'Mon titre warning',
                'Mon message warning',
                Notification::TARGET_GROUPS,
                Notification::SEVERITY_WARNING,
                array('ROLE_ADMIN','ROLE_MANAGER'),
                'MY_TAG'
            );
        ```
        