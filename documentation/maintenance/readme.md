Mode Maintenance FiducialCore
======

Sommaire:
-------------

- [Présentation](#toc_2)
- [Configurer le mode maintenance](#toc_3)

Présentation:
-------------

- Le FiducialCore est livré avec un mode maintenance, pour bloquer l'utilisation de l'application si besoin est.
- Si le mode maintenance est actif, **toute requete vers l'application sera interceptée** pour afficher la page de maintenance.
- Le mode maintenance **n'affecte que l'environnement production** de Symfony2 (c'est a dire que l'application reste disponible sur les autres environnements: dev, test, etc.)
- En environnement poduction, **il est necessaire de vider les caches Symfony2** pour la prise en compte du mode maintenance
    - `$ php app/console cache:clear`
    
Configurer le mode maintenance:
----------------------------------------------------

- Les options de configuration disponibles sont les suivantes (avec leur valeurs par défaut):

```
#app/config/fiducial_core.yml
fiducial_core:
    maintenance:
        #active Core maintenance
        enable_maintenance: false
        #date de retour au fonctionnement
        working_back_at: null
        #template de page de maintenance
        template: 'FiducialCoreBundle:Maintenance:index.html.twig'

```

- Vous pouvez:
    - indiquer une date de retour au fonctionnement (si c'est laissé à *null*, FiducialCore indiquera que l'application sera disponible dès que possible)
    - surcharger la template de la page de maintenance, par exemple:
    
    ```
    #app/config/fiducial_core.yml
    fiducial_core:
        maintenance:
            enable_maintenance: true
            working_back_at: "demain, 08h00"
            template: 'MyBundle:Maintenance:index.html.twig'
    ```