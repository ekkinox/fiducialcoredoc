FAQ / Best Practices
======

Vous trouverez ici:
- une FAQ pour répondre aux problématiques généralement soulevées lors de l'utilisation du FiducialCore
- des Best Practices (et retours sur expérience) pour une bonne utilisation de ce dernier et de Symfony2 en général

## FAQ 

- [Bases](best_practices/basics/): 
    - [Bien commencer avec le FiducialCore](best_practices/basics/basics.md) : *comment bien commencer avec le contexte FiducialCore*
    - [Créer un formType personalisé](best_practices/basics/new_form_type.md) : *comment créer un type de champ personalisé*

## Best Practices

- [Oracle](best_practices/oracle/):
    - [Gestion des dates sous Oracle](best_practices/oracle/dates.md): *récupérer, insérer, mettre à jour et afficher les dates sous Oracle OCI*
	
- [Doctine](best_practices/doctrine/):
    - [Mapping externe](best_practices/oracle/dates.md): *Créer et mapper des entités doctrine depuis une BDD existante (Oracle, SQLServer, etc.)*
	
- [Qualité](best_practices/quality/)
    - [Code Sniffer](best_practices/quality/phpcs.md): *Comment utiliser PHP Code Sniffer pour le maintient de la qualité du code*


