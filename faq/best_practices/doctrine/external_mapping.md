Bonnes Pratiques : Créer et mapper des entités doctrine depuis une BDD existante
======

- Certaines applications utilisent des tables externes (par exemple depuis Talentia) pour récupérer de l'information. Il faut alors créer des entités qui correspondent à ces tables. Nous allons utiliser un exemple.

Exemple : à partir d'une table Talentia (Oracle)
-------------

- Voici une table Oracle utilisée dans Talentia. Notre objectif est de la créer dans un projet Symfony2 qui utilise une base PostgreSQL.

    ```
    CREATE TABLE RH.ORG_POSTE
    (
      POSTE              VARCHAR2(12 BYTE),
      ORGANISATION       VARCHAR2(12 BYTE)          NOT NULL,
      MATRICULE          VARCHAR2(8 BYTE)           NOT NULL,
      N1                 VARCHAR2(12 BYTE),
      N2                 VARCHAR2(12 BYTE),
      N3                 VARCHAR2(12 BYTE),
      N4                 VARCHAR2(12 BYTE),
      N5                 VARCHAR2(12 BYTE),
      N6                 VARCHAR2(12 BYTE),
      N7                 VARCHAR2(12 BYTE),
      N8                 VARCHAR2(12 BYTE),
      DATE_EFFET         DATE                       NOT NULL,
      DATE_FIN           DATE,
      MATRI_PRECEDENT    VARCHAR2(8 BYTE),
      MATRI_SUPPLEANT    VARCHAR2(8 BYTE),
      MOTIF_ENTREE       VARCHAR2(12 BYTE),
      MOTIF_DEPART       VARCHAR2(12 BYTE),
      REF_DEMANDE        VARCHAR2(12 BYTE),
      FLAG_MUTATION      VARCHAR2(1 BYTE),
      SEQ_ETABLISSEMENT  NUMBER(15),
      SEQ_MUTATION       NUMBER(15),
      TYPE_MOTIF_ENTREE  VARCHAR2(2 BYTE),
      TYPE_MOTIF_DEPART  VARCHAR2(2 BYTE),
      SEQ_AFFECT         NUMBER(10),
      USERID_ARCOLE      VARCHAR2(30 BYTE),
      DATE_ARCOLE        DATE,
      SEQ_MAT_EMPLOI     NUMBER(15)
    )
    ```

- 1) Notre première étape est de créer la même table dans PostgreSQL. Vous devez convertir la requête SQL Oracle dans une syntaxe PostgreSQL avec quelques ajouts :

    ```
    CREATE TABLE org_poste
    (
      id                 integer NOT NULL,
      POSTE              character varying(12),
      ORGANISATION       character varying(12)          NOT NULL,
      MATRICULE          character varying(8)           NOT NULL,
      N1                 character varying(12),
      N2                 character varying(12),
      N3                 character varying(12),
      N4                 character varying(12),
      N5                 character varying(12),
      N6                 character varying(12),
      N7                 character varying(12),
      N8                 character varying(12),
      DATE_EFFET         DATE                       NOT NULL,
      DATE_FIN           DATE,
      MATRI_PRECEDENT    character varying(8),
      MATRI_SUPPLEANT    character varying(8),
      MOTIF_ENTREE       character varying(12),
      MOTIF_DEPART       character varying(12),
      REF_DEMANDE        character varying(12),
      FLAG_MUTATION      character varying(1),
      SEQ_ETABLISSEMENT  NUMERIC(15),
      SEQ_MUTATION       NUMERIC(15),
      TYPE_MOTIF_ENTREE  character varying(2),
      TYPE_MOTIF_DEPART  character varying(2),
      SEQ_AFFECT         NUMERIC(10),
      USERID_ARCOLE      character varying(30),
      DATE_ARCOLE        DATE,
      SEQ_MAT_EMPLOI     NUMERIC(15),
     CONSTRAINT option_pkey_org_poste PRIMARY KEY (id)
    )
    WITH (
     OIDS=FALSE
    );
    ALTER TABLE org_poste
     OWNER TO "userDev";
    ```

- 2) Convertissez cette nouvelle table en mapping YML, en utilisant la commande suivante :

    ```
    php app/console doctrine:mapping:convert yml ./src/Gde/TalentiaBundle/Resources/config/doctrine/metadata/orm --from-database --force
    ```
    
Ceci convertira votre base PostgreSQL complète en fichiers yml. Ces derniers seront placée dans le dossier : /src/Gde/TalentiaBundle/Resources/config/doctrine/metadata/orm.

- 3) Toutes les tables de PostgreSQL ont été mappées. Mais vous n'avez pas besoin de toutes : supprimez les tables inutiles directement depuis votre IDE favori.

- 4) Convertissez vos fichiers de mapping en entités en utilisant la ligne de commande suivante :

    ```
    php app/console doctrine:mapping:import GdeTalentiaBundle annotation
    ```
    
Ces nouvelles entités seront placés dans le dossier GdeTalentiaBundle, et sera piloté par annotations.

- 5) Supprimez les entités dont vous n'avez pas besoin depuis votre IDE. Une fois encore, toutes les tables depuis PostgreSQL ont été converties en entitées, et vous pourriez ne pas avoir besoin de toutes.

- 6) Adaptez les annotations des nouvelles entités :

    ```
    <?php
    /**
     * OrgPoste
     *
     * @ORM\Table(name="rh.org_poste")
     * @ORM\Entity(repositoryClass="Gde\TalentiaBundle\Entity\OrgPosteRepository")
     */
     ```
     Nous changeons le nom de la table pour pointez sur le nom de la table Oracle.
     
     ```
     <?php
     /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="POSTE", type="string", length=20)
     */
    private $poste;
     ```
    Nous choisissons un attribut qui représentera l'ID pour Doctrine. Vous aurez _toujours_ besoin d'un ID, il référence chaque entité. Cet ID doit être unique afin d'être sûr qu'aucun problème ne surgisse.
 
     ```
     <?php
     /**
     * @var string
     *
     * @ORM\Column(name="ORGANISATION", type="string", length=12, nullable=false)
     */
    private $organisation;
    ```
    Pour chaque attribut, nous réécrivons le nom en MAJUSCULES, remplaçons le type date par string, et vérifions que le type est cohérent.

- 7) Générez les getters et les setters pour les nouvelles entités en utilisant la commande suivante :

    ```
    app/console doctrine:generate:entities GdeTalentiaBundle:XXX
    ```

- 8) Créez une classe repository pour chaque nouvelle entité (org_poste pour notre exemple), et écrivez vos méthodes de recherche ici.

- 9) Ajoutez au TalentiaManager (TalentiaBundle/Manager/TalentiaManager.php) une fonction raccourci pour accéder au repository :

    ```
    <?php
    /**
     * @param null $userId
     * @param null $toReturn
     * @return mixed
     */
    public function getOrgPostes($userId = null, $toReturn = null)
    {
        return $this
            ->em
            ->getRepository('GdeTalentiaBundle:OrgPoste')
            ->getOrgPostes($userId, $toReturn);
    }
    ```
