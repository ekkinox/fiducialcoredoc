Bonnes Pratiques : Ajouter un nouveau type de formulaire
======

Exemple : Basé sur un champ Talentia (entité mapping externe)
-------------
- Dans ce tutorial, nous utiliserons le champ "CATEGORIE" de Talentia en guise d'exemple.

- 1) Ajoutez votre nouveau service dans le fichier TalentiaBundle\Resources\config\services.yml

    ```
        gde_talentia.form.type.categories_list:
            class: Gde\TalentiaBundle\Form\Type\CategoriesList
            arguments:
                - "@gde_talentia.talentia_manager"
            tags:
                - { name: form.type, alias: talentia_categories_list }
    ```

Ceci ajoutera le service **gde_talentia.form.type.categories_list**, basé sur la classe Formulaire Gde\TalentiaBundle\Form\Type\CategoriesList, et utilise le TalentiaManager.

- 2) Créez votre classe de Formulaire en utilisant le chemin que vous avez défini au préalable dans votre déclaration de service. Dans notre exemple, ce sera : Gde\TalentiaBundle\Form\Type\CategoriesList.

    ```
    namespace Gde\TalentiaBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Gde\TalentiaBundle\Manager\TalentiaManager;
    
    class CategoriesList extends AbstractType
    {
        /**
         * @var TalentiaManager
         */
        private $provider;
    
        /**
         * @param TalentiaManager $provider
         */
        public function __construct(TalentiaManager $provider)
        {
            $this->provider = $provider;
        }
    
        /**
         * @param OptionsResolver $resolver
         */
        public function configureOptions(OptionsResolver $resolver)
        {
            $resolver->setDefaults(
                array(
                    'choices' => $this->provider->remapData(
                        $this->provider->getRefCategoryStatuses()
                    ),
                )
            );
        }
    
        /**
         * @return string
         */
        public function getParent()
        {
            return 'choice';
        }
    
        /**
         * @return string
         */
        public function getName()
        {
            return 'talentia_categories_list';
        }
    }
    ```

- 3) Ajoutez le nouveau champ de formulaire au fichier Form type souhaité.
Dans notre exemple, ce sera Gde\ContractBundle\Form\CriteriaType.php. Ajoutez le champ personnalisé à l'entité.
Vous avez juste à adapter le code source suivant à votre contexte :

```
 public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('backendList', 'talentia_categories_list', array(
                'placeholder' => 'Choose a category',
            ));
    }
 ```
 
 - 4) Si vous n'avez pas déjà la vue TalentiaBundle\Resources\views\Form\talentia_fields.html.twig, créez-la.
 
 - 5) Dans talentia_fields.html.twig, ajoutez un bloc qui spécifie le type de champ personnalisé.
 
 ```
 {% block talentia_categories_list_widget %}
     {% spaceless %}
         {% if expanded %}
             <ul {{ block('widget_container_attributes') }}>
                 {% for child in form %}
                     <li>
                         {{ form_widget(child) }}
                         {{ form_label(child) }}
                     </li>
                 {% endfor %}
             </ul>
         {% else %}
             {{ block('choice_widget') }}
         {% endif %}
     {% endspaceless %}
 {% endblock %}
 ```
 Dans cet exemple, cela affichera un selecteur pour les catégories.
 
 - 6) Ajoutez dans votre fichier app/config/config.yml votre vue twig.
 
     ```
     # Twig Configuration
     twig:
         debug:            "%kernel.debug%"
         strict_variables: "%kernel.debug%"
         form_themes:
             - 'GdeTalentiaBundle:Form:talentia_fields.html.twig'
     ```