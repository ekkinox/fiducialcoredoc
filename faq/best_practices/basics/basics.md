Bonnes Pratiques : Les bases de Symfony2 et du FiducialCore
======

Rappels des fondamentaux SF2
-------------
- SF2 propose la création de bundles, qui contiennent des entités (comme les Packages en Orienté Objet).
- Les entités (représentés par des fichiers php dans votre application) sont semblables à des classes en Orienté Objet. Elles seront alors écrites en base de données grâce à un ORM, qui se charge de la conversion (mapping). Dans notre cas, il s'agit de Doctrine.
- Chaque entité contient des méthodes et des attributs.

Créer un bundle
-------------
- Afin de générer un bundle, utilisez la commande suivante :

    ```
    php app/console generate:bundle
    ```
    
    Suivez les étapes. Il vous sera demandé de spécifier le chemin complet de votre bundle (par exemple : Gde/ContractBundle sera écrit de cette manière : GdeContractBundle).
    Vous devez vous situer dans le répertoire racine de votre projet afin que la commande app/console soit reconnue.

Créer une entité
-------------
- Afin de générer une entité, utilisez la commande suivante :

    ```
    php app/console fiducialcore:generate:entity
    ```
    
    Suivez les étapes. Il vous sera demandé de spécifier le bundle qui recevra la nouvelle entité, les attributs de cette dernière (il vous sera possible de les changer par la suite en modifiant directement le fichier php de l'entité), et si vous souhaitez créer le repository associé à l'entité (réponsez oui, ainsi le fichier sera déjà créé et prêt à l'emploi le jour où vous en aurez besoin).

Créer un CRUD
-------------
- Afin de générer le CRUD Fiducial (Creating, Reader, Updating & Deleting, actions à effectuer sur une entité stockée en base de données via l'application), utiliser la commande suivante :

    ```
    php app/console fiducialcore:generate:crud
    ```

    Il vous sera demandé de spécifier l'entité dont les CRUD seront générés (par exemple : Gde/ContractBundle/Entity/User deviendra : GdeContractBundle:User), ainsi que la route qui sera utilisée pour accéder à ces vues.
    
Mise à jour de la base de données
-------------
- Afin de MAJ la base de données, utilisez la commande suivante :

    ```
    php app/console doctrine:schema:update --force --dump-sql
    ```