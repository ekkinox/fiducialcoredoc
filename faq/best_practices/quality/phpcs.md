Bonnes Pratiques : Toujours pousser un code source php normé et propre
======

Utilisation de l'outil PHP Code Sniffer (phpcs)
-------------
- En utilisant l'outil phpcs, vous pouvez transformer votre code php en un code aux normes Symfony2, propre, et indenté correctement.

    ```
    sudo php-cs-fixer fix PATHTOCLEAN --level-symfony -vvv
    ```

- Dans cet exemple, nous nettoyons les fichiers php du dossier src.

    ```
    sudo php-cs-fixer fix /app/userDev/YOU/src --level-symfony -vvv
    ```

- La commande étant très longue et fastidieuse à mémoriser, un alias a été créé et est utilisable de la façon suivante :

    ```
    phpcs /app/userDev/YOU/src
    ```
    
- Note : Lorsque vous utilisez phpcs, appliquez **toujours** phpcs sur le dossier src de votre projet, et **jamais** sur le FiducialCore. En effet, le FiducialCore ne doit **jamais** être modifié dans un projet externe à ce dernier !