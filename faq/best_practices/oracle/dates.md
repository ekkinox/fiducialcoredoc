Bonnes Pratiques : Les dates sous Oracle
======

### L'écouteur pour Oracle

Doctrine fournit un service sur l'après connection (postConnect) à une base Oracle. 
Ce service disposé dans un écouteur de l'évennement postConnect permet de définir pour la session
en cours le format de date qu'Oracle va nous fournir et qu'on devra lui donner.

```
ALTER SESSION 
SET 
  NLS_TIME_FORMAT = 'HH24:MI:SS' 
  NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS' 
  NLS_TIMESTAMP_FORMAT = 'YYYY-MM-DD HH24:MI:SS' 
  NLS_TIMESTAMP_TZ_FORMAT = 'YYYY-MM-DD HH24:MI:SS TZH:TZM' 
  NLS_NUMERIC_CHARACTERS = '.,'
```

Pour l'activer, il suffit de rajouter dans son app/config/services.yml l'écouteur suivant :

```
services:
    my.oracle.listener:
        class: Doctrine\DBAL\Event\Listeners\OracleSessionInit
        tags:
            - { name: doctrine.event_listener, event: postConnect, connection: oracleManager }
```

Notes :
- Si vous oubliez de spécifier la connection (manager), il prendra celle par défaut (postgresSQL).
- le nom (my.oracle.listener) de l'écouteur est arbitraire. 

Désormais, toutes les dates sortiront avec le même format quelle que soit la base de données Oracle.  

### Lier les champs dates

Il faut simplement déclarer ses dates comme suit :
    
```
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="MY_DATE", type="string", length=50)
     * @GRID\Column(type="datetime")
     */
    private $myDate;
```


Nous avons fixé le format plus haut avec l'écouteur. Il faut le précicer à nos entités:

```
    // date format to generate datetime objects 
    const DATE_FORMAT = 'Y-m-d H:i:s';
```

Si vous avez d'autres entités, vous pouvez faire une référence au format d'une seule :


```
    public class anotherEntity {
         const DATE_FORMAT = MainEntity::DATE_FORMAT;
```

Maintenant que nous avons le format des dates, on construit nos DateTime avec un postLoad dans l'entité :

```
    /**
     *
     * when load date from oracle, create a new datetime
     *
     * @ORM\PostLoad
     */
    public function convertDateFromOracle()
    {
        // always check if variable is a string
        if (is_string($stringDate = $this->getMyDate())) {
            $this->setMyDate(
                \DateTime::createFromFormat(self::DATE_FORMAT, $stringDate)
            );
        }
         
         and same for other date field...
 
     }
```

Une fois le DateTime manipulé, mis à jour ou créé, il faut le reconvertir en une chaîne de caractères pour Oracle. 
Pour cela, on utilise un PrePersist et PreUpdate :

```
    /**
     *
     * when persist or update, convert datetime to string
     * for oracle (auto convert in oracle date)
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function convertDateToOracle()
    {
        // always check if variable is a DateTime
        if (is_a($datetime = $this->getMyDate(), 'DateTime')) {
            $this->setMyDate($datetime->format(self::DATE_FORMAT));
        }
                
        and same for other date field...
    }
```

Oracle va faire seule la convertion de la chaine de caractères en date Oracle.  

### Afficher les dates

- Dans les templates twig

Il suffit d'ajouter à son entité la fonction twig date avec le format en paramètre :

```
    {{ entity.dteXXX | date("d/m/Y") }}
```
    
- Dans les grids
    
Nous avons besoin de récupérer dans le controller la colonne et ensuite 
d'appliquer le bon format :

```
    $gridBuilder->getColumn('dteXXX')->manipulateRenderCell(
        function ($value, $row, $router) {
            if ($row->getField('dteXXX') !='') {
                $dateT = \DateTime::createFromFormat(MainEntity::DATE_FORMAT, $row->getField('dteXXX'));
                return $dateT->format('d/m/Y');
            }
        }
    );
```
    
Note : On fait référence au format de l'entité vu plus haut pour créer notre DateTime et ensuite formater
comme voulu notre date.
