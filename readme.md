README : Fiducial Core
======

Description
-----------------

Fiducial Core est un ensemble de fonctionnalités ayant pour but d'accélérer, cadrer et automatiser le developpement d'applications PHP pour Fiducial autour du framework [Symfony2](https://symfony.com/).

##### Il propose:
- un front natif responsive (basé [Boostrap 3](http://getbootstrap.com/)),
- des outils de génération de code (bundles, entités et cruds) préparés au couplage avec le Fiducial Core,
- des contextes de programmation objet PHP (classes, interfaces, traits, etc.) prêts à l'emploi,
- la compatibilité par défaut aux webservices REST
- et de nombreuses fonctionalités capitalisées nécessaires à la contruction d'applications PHP riches (menus, gestion utilisateur, grilles, recherche globale, composants IHM fonctionnels, etc.).


Documentation technique
-----------------

Un espace de documentation *(installation, doc technique)* est [disponible ici](documentation/).


FAQ / Best Practices
-----------------

Un espace de FAQ / Best Practices est [disponible ici](faq/).
   

Informations techniques
------------------------

##### Composants (Bundles):
- Fiducial\CoreBundle : bundle principal
- Fiducial\UserBundle : bundle de gestion utilisateurs

@todo : bundle de monitoring

##### Pré-requis:
- PHP (version > 5.4.40)
- PHP : apc, ctype, curl, fileinfo, json, mbstring, Reflection, PDO, SimpleXML, xml, xmlreader, xmlwriter  
- Symfony2 (développé sous LTS 2.7.X)
- NPM : Less, PhantomJS

##### Le Fiducial Core a été testé pour fonctionner avec les SGBD suivants:
- PostgreSQL
- MySQL
- SQLServer
- Oracle (versions 8, 10, 11)

##### Il est dépendant des bundles tiers suivants:
- [MopaBootstrapBundle](https://github.com/phiamo/MopaBootstrapBundle)
- [KnpMenuBundle](https://github.com/KnpLabs/KnpMenuBundle)
- [KnpPaginatorBundle](https://github.com/KnpLabs/KnpPaginatorBundle)
- [JMSSerializerBundle](https://github.com/schmittjoh/JMSSerializerBundle)
- [FOSUserBundle](https://github.com/FriendsOfSymfony/FOSUserBundle)
- [FOSJsRoutingBundle](https://github.com/FriendsOfSymfony/FOSJsRoutingBundle)
- [FOSRestBundle](https://github.com/FriendsOfSymfony/FOSRestBundle)
- [APYDataGridBundle](https://github.com/Abhoryo/APYDataGridBundle)

Liste exhaustive des dépendances dans le composer.json

Tests
-------

##### Outils de testing (unitaires, fonctionnels)
- [Behat](http://docs.behat.org/en/v2.5/)
- [Mink](http://mink.behat.org/en/latest/)
- [PhantomJS](http://phantomjs.org/)
- [PHPUnit](https://phpunit.de/)
- [Atoum](http://docs.atoum.org/fr/latest/)

A des fins d'intégration continue

Auteurs
-------
Jonathan VUILLEMIN ([jvuillemin@norsys.fr](mailto:jvuillemin@norsys.fr))

Version
-------
v 1.0.0
